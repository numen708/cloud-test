from flask_cors import CORS
from forecast.main import forecastApi
from flask import Flask, request, jsonify
app = Flask(__name__)
cors = CORS(app)

# set FLASK_APP=server.py && set FLASK_DEBUG=1 && flask run
def registerBlueprints(app):
	app.register_blueprint(forecastApi)

registerBlueprints(app)

@app.route('/')
def hello():
    dictionary = {
        'forecast': 'forecast wells'
    }

    return jsonify(dictionary)

# not sure what this does or if i need it
# if __name__ == '__main__':
#     app.run(debug=True)

# modify test xuyan

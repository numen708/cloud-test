import copy
import numpy as np

##
def AveNonzero(Array):
    return np.mean(Array[Array!=0])


##
def movingaverage(interval, window_size): ### return 
    ma = np.zeros(interval.shape)
    for i in range(len(interval) - window_size + 1):
        ma[i] = (AveNonzero(interval[i:i + window_size]))
    for i in range(len(interval) - window_size + 1, len(interval)):
        ma[i] = (AveNonzero(interval[i + 1 - window_size:i+1]))
    return ma


##
def movingaverage_back(interval, window_size):
    ma = np.zeros(interval.shape)
    for i in range(window_size - 1):
        ma[i] = (AveNonzero(interval[i:i + window_size]))
    for i in range(window_size - 1, len(interval)):
        ma[i] = (AveNonzero(interval[i + 1 - window_size: i+1]))
    return ma


##
def movingaverage_f_b(interval, window_size):
    ma_f = movingaverage(interval, window_size)
    ma_b = movingaverage_back(interval, window_size)
    return (ma_f + ma_b)/2


##
def remove_consec(peak, valley, rate):
    ((np.array(peak[:-1]).reshape(1,-1) < np.array(valley).reshape(-1,1)) & (np.array(peak[1:]).reshape(1,-1) > np.array(valley).reshape(-1,1))).any(axis=0)
    # remove 2 consecutive peaks or valleys
    peak_del_index = []
    for i in range(len(peak)-1):
        g = 0
        for j in valley:
            if peak[i] < j < peak[i+1]:
                g = 1
                # break
        if g == 0:
            if rate[peak[i]] < rate[peak[i+1]]:
                peak_del_index.append(i)
            else:
                peak_del_index.append(i+1)
 
    valley_del_index = []
    for i in range(len(valley)-1):
        g = 0
        for j in peak:
            if valley[i] < j < valley[i+1]:
                g = 1
        if g == 0:
            if rate[valley[i]] < rate[valley[i+1]]:
                valley_del_index.append(i+1)
            else:
                valley_del_index.append(i)
    
    peak_new = []
    
    for i in range(len(peak)):
        if i not in peak_del_index:
            peak_new.append(peak[i])
            
    valley_new = []
    
    for i in range(len(valley)):
        if i not in valley_del_index:
            valley_new.append(valley[i])
    
    return peak_new, valley_new

#def remove_consec(peak, valley, rate):
#    ####### remove peaks
#    peak_fail_list = ((peak[:-1].reshape(1,-1) < valley.reshape(-1,1)) & (peak[1:].reshape(1,-1) > valley.reshape(-1,1))).any(axis=0)
#    ind = np.argwhere(peak_fail_list==False).reshape(-1,)
#    peak_keep_ind = np.ones(peak.shape[0],dtype=bool)
#    if ind.shape[0]> 0:
#        i = 0
#        while(i < ind.shape[0]):
##            print(i)
#            comp_list = [ind[i], ind[i]+1]
#            if i < ind.shape[0]-1:
#                while(ind[i+1] == ind[i]+1):
#                    comp_list += [ind[i+1]+1]
#                    i += 1
#                    if i == ind.shape[0]-1:
#                        break
#                
#            i +=1
#            keep_ind = np.argmax(rate[comp_list])
#            peak_keep_ind[comp_list] = 0
#            peak_keep_ind[comp_list[keep_ind]] = 1
#            
#    final_peak = peak[peak_keep_ind]
#    ############# valley
#    valley_fail_list = ((valley[:-1].reshape(1,-1)<final_peak.reshape(-1,1)) & (valley[1:].reshape(1,-1)>final_peak.reshape(-1,1))).any(axis=0)
#    ind = np.argwhere(valley_fail_list==False).reshape(-1,)
#    valley_keep_ind = np.ones(valley.shape[0],dtype = bool)
#    if ind.shape[0]> 0:
#        i = 0
#        while(i < ind.shape[0]):
##            print(i)
#            comp_list = [ind[i], ind[i]+1]
#            if i < ind.shape[0]-1:
#                while(ind[i+1] == ind[i]+1):
#                    comp_list += [ind[i+1]+1]
#                    i += 1
#                    if i == ind.shape[0]-1:
#                        break
#                
#            i +=1
#            keep_ind = np.argmin(rate[comp_list])
#            valley_keep_ind[comp_list] = 0
#            valley_keep_ind[comp_list[keep_ind]] = 1
#    
#    final_valley = valley[valley_keep_ind]
#    return final_peak, final_valley
######################################################################################
#### this is the new remove_peak function
def remove_peak(peak, valley, rate, rate_orig, threshold, threshold2):
    # remove flat peaks
    if len(peak) > 0 and len(valley) > 0:
        if valley[0] < peak[0]:
            v_compare = valley[1:]
        else:
            v_compare = valley
        
        peak_remove_index = []
        
        # print(peak)
        
        for i in range(1, len(peak)):
            # skip the first peak
            # ratio = rate_orig[peak[i]] / rate_orig[v_compare[i-1]]
            ratio = rate[peak[i]] / rate[v_compare[i-1]]
            delta = (rate[peak[i]] - rate[v_compare[i-1]])
            
            p_5 = np.percentile(rate, 10)         
            p_95 = np.percentile(rate, 90)
            
            if ratio < threshold or rate[peak[i]] < np.percentile(rate, 30) or delta/(p_95-p_5 + 1) <  max(threshold2 - 0.05, threshold2 - (0.05/9) * (peak[i] - v_compare[i-1] -1)):
                peak_remove_index.append(i)
        
        # print('Removed Peak:', [peak[i] for i in peak_remove_index])
        
        peak_new = [peak[i] for i in range(len(peak)) if i not in peak_remove_index]
                
        return peak_new, valley
    else:
        return peak, valley
  
###########################################################################################
##
def remove_nearby_peak(peak, valley, rate):
    # remove peaks(valleys) that close to each other.
    # two peaks (valleys) with distance within 6 data points, remove the lower (higher) one.
    if len(peak) > 0 and len(valley) > 0:
        peak_rm_ind = []
        for i in range(len(peak)-1):
            if abs(peak[i] - peak[i+1]) < 6:
                if rate[peak[i]] < rate[peak[i+1]]:
                    peak_rm_ind.append(i)
                else:
                    peak_rm_ind.append(i+1)
        
        valley_rm_ind = []
        for i in range(len(valley)-1):
            if abs(valley[i] - valley[i+1]) < 6:
                if rate[valley[i]] > rate[valley[i+1]]:
                    valley_rm_ind.append(i)
                else:
                    valley_rm_ind.append(i+1)
                    
        peak_new = [peak[i] for i in range(len(peak)) if i not in peak_rm_ind]
        valley_new = [valley[i] for i in range(len(valley)) if i not in valley_rm_ind]
        
        # print(peak_rm_ind, valley_rm_ind)
        
        return peak_new, valley_new
    else:
        return peak, valley




def find_peak_valley_vec(t, rate, T1, T2):
    # find peaks and valleys by 6 nearby derivatives (3 before and 3 after).
    
    if len(t) < 7:
        peak = [np.argmax(rate)]
        valley = []
    else:
        rate_orig = copy.copy(rate)
        rate = movingaverage_f_b(rate, 2)
               
        peak = np.array([]).astype(np.int32)
        valley = np.array([]).astype(np.int32)
        # first point
        if sum(rate[0] >= np.array([rate[1], rate[2], rate[3]])) == 3:
            peak = np.append(peak, 0)
        elif sum(rate[0] <= np.array([rate[1], rate[2], rate[3]])) == 3:
            valley = np.append(valley, 0)
            
        # second point
        if sum(rate[1] >= np.array([rate[0], rate[1], rate[2], rate[3]])) == 4:
            peak = np.append(peak, 1)
        elif sum(rate[1] <= np.array([rate[0], rate[1], rate[2], rate[3]])) == 4:
            valley = np.append(valley, 1)  
            
        # third point
        if sum(rate[2] >= np.array([rate[0], rate[1], rate[1], rate[2], rate[3]])) == 5:
            peak = np.append(peak, 2)
        elif sum(rate[2] <= np.array([rate[0], rate[1], rate[1], rate[2], rate[3]])) == 5:
            valley = np.append(valley, 2)
            
        # fourth to last fourth
        rate_bf3= rate[0:-6]
        rate_bf2 = rate[1:-5]
        rate_bf1 = rate[2:-4]
        rate_0 = rate[3:-3]
        rate_af1 = rate[4:-2]
        rate_af2 = rate[5:-1]
        rate_af3 = rate[6:]
        
        # print(np.array([rate_bf3, rate_bf2, rate_bf1, rate_af1, rate_af2, rate_af3]))
        
        peak_ind = np.sum(rate_0 >= np.array([rate_bf3, rate_bf2, rate_bf1, rate_af1, rate_af2, rate_af3]), axis = 0)
        peak = np.append(peak, np.add(np.where( peak_ind == 6), 3))
        
        valley_ind = np.sum(rate_0 <= np.array([rate_bf3, rate_bf2, rate_bf1, rate_af1, rate_af2, rate_af3]), axis = 0)
        valley = np.append(valley, np.add(np.where( valley_ind == 6), 3))
        
        # last third
        if sum(rate[-3] >= np.array([rate[-6], rate[-5], rate[-4], rate[-2], rate[-1]])) == 5:
            peak = np.append(peak, len(rate) - 3)
        elif sum(rate[-3] <= np.array([rate[-6], rate[-5], rate[-4], rate[-2], rate[-1]])) == 5:
            valley = np.append(valley, len(rate) - 3)
            
        # last second
        if sum(rate[-2] >= np.array([rate[-5], rate[-4], rate[-3], rate[-1]])) == 4:
            peak = np.append(peak, len(rate) - 2)
        elif sum(rate[-2] <= np.array([rate[-5], rate[-4], rate[-3], rate[-1]])) == 4:
            valley = np.append(valley, len(rate) - 2)
     
        # last one
        if sum(rate[-1] >= np.array([rate[-4], rate[-3], rate[-2]])) == 3:
           peak =  np.append(peak, len(rate) - 1)
        elif sum(rate[-1] <= np.array([rate[-4], rate[-3], rate[-2]])) == 3:
           valley = np.append(valley, len(rate) - 1)
            
#        peak, valley = remove_nearby_peak(peak, valley, rate)
#        # print('after remove nearby peaks:', peak)      
#        peak, valley = remove_consec(peak, valley, rate)
#        peak, valley = remove_peak(peak, valley, rate, rate_orig, T1, T2)
#        #print('after remove bad peaks:', peak)
#        peak, valley = remove_consec(peak, valley, rate)
#        peak, valley = remove_consec(peak, valley, rate)
#        #print('final peaks:', peak)
        peak, valley = remove_nearby_peak(peak, valley, rate)        
        for i in range(100):
            new_peak, new_valley = remove_consec(peak, valley, rate)
            if new_peak == peak and new_valley == valley:
                break
            else:
                peak = new_peak
                valley = new_valley
                
        peak, valley = remove_peak(peak, valley, rate, rate_orig, T1, T2)
        
        for i in range(100):
            new_peak, new_valley = remove_consec(peak, valley, rate)
            if new_peak == peak and new_valley == valley:
                break
            else:
                peak = new_peak
                valley = new_valley
        # find higher points near the peaks to be new peaks.    
        for i in range(len(peak)):
            this_p = peak[i]
            peak_candidate = [this_p-1, this_p, this_p+1]
            peak_candidate = [i for i in peak_candidate if i in range(len(t))]
            peak_candidate_rate = [rate_orig[i] for i in peak_candidate]
            peak_new = peak_candidate[np.argmax(peak_candidate_rate)]
            
            peak[i] = peak_new
            
    return peak, valley
    # the returned peaks and valleys are the index of input sequence.

##
def second_max(numbers):
    ind1 = 0
    ind2 = 0
    m1 = m2 = float('-inf')
    for i in range(len(numbers)):
        if numbers[i] > m2:
            if numbers[i] >= m1:
                m1, m2 = numbers[i], m1
                ind2 = ind1
                ind1 = i
            else:
                m2 = numbers[i]
                ind2 = i
    return [ind1, m1], [ind2, m2]


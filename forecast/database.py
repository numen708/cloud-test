import os
from pymongo import MongoClient

DB_NAME = os.environ.get("DB_NAME", default="development")
DB_USERNAME = os.environ.get("DB_USERNAME", default="devAdmin")
DB_PASSWORD = os.environ.get("DB_PASSWORD", default="Redcar123*")
DB_CLUSTER = os.environ.get("DB_CLUSTER", default="inpt-cluster-ukmek.gcp.mongodb.net")

def connect():
	client = MongoClient('mongodb+srv://'+DB_USERNAME+':'+DB_PASSWORD+'@'+DB_CLUSTER+'/admin?retryWrites=true')
	return client

def getDb(client):
	return client[DB_NAME]

def disconnect(client):
	client.close()
import copy
import numpy as np
from forecast.helper import common_number
from forecast.skeleton_DCA_v3 import get_DCA
from scipy.optimize import differential_evolution

days_in_year  = common_number['days_in_year']
def segment_arps_4_wp_no_data_para(data, peak_idx, label, this_range):
    det = get_DCA()
    data_filter = det.transform_s['default'](data,peak_idx)
    data_ap = det.transform_s['afterpeak'](data,peak_idx)
    ret_dict = dict()
    if label==1:
        return ret_dict
    elif label ==2:
        ret_dict['best_q0'] = data_filter[0,1]
        ret_dict['best_minus_t0_t_first'] = data_filter[0,0] - data[0,0]
    elif label in [3,5]:
        q_peak = data_ap[0,1]
        ret_dict['best_q0'] = data_filter[0,1]
        ret_dict['best_minus_t0_t_first'] = data_filter[0,0] - data[0,0]
        ret_dict['best_q_peak'] = q_peak
        ret_dict['best_minus_t_peak_t0'] = peak_idx - data_filter[0,0]
    elif label in [4,6]:
        q_peak = data_ap[0,1]
        def alpha(p,*args):
            D1 = p
            b1 = 2
            trans_data, q_peak, t_peak = args
            t = trans_data[:,0]
            y_true = trans_data[:,1]
            pred = q_peak/np.power(1+b1*D1*(t-t_peak),1/b1)
            return np.mean(np.square(y_true-pred))
        args = data_ap, q_peak, peak_idx
        result = differential_evolution(alpha, args = args, bounds = this_range,popsize = 15)
        ret_dict['best_q0'] = data_filter[0,1]
        ret_dict['best_minus_t0_t_first'] = data_filter[0,0] - data[0,0]
        ret_dict['best_q_peak'] = q_peak
        ret_dict['best_minus_t_peak_t0'] = peak_idx - data_filter[0,0]
        ret_dict['best_D1'] = result.x[0]
        ret_dict['best_D1_eff'] = copy.deepcopy((1-np.power(1+days_in_year*2*result.x[0],-1/2)))
    
    return ret_dict

    

no_data_para_s = {
                  'segment_arps_4_wp': segment_arps_4_wp_no_data_para
                  }
###################################################################################


def segment_arps_4_wp_get_range(para_dict, label):
    ret = [(1e-5,100)]
    if 'D1' in para_dict.keys():
        ret[0] = tuple(para_dict['D1'])
        
    return ret
    
get_range_s = {
               'segment_arps_4_wp':segment_arps_4_wp_get_range
               }
#############################################################
class get_no_data:
    def __init__(self):
        self.no_data_para_s = no_data_para_s
        self.get_range_s = get_range_s
    
    def get_range(self, model_name, para_dict, label):
        return self.get_range_s[model_name](para_dict, label)

    def get_paras(self, data, t_peak, label, model, this_range):
        return self.no_data_para_s[model](data,t_peak,label, this_range)

import json
import datetime
import numpy as np
import pandas as pd
import pprint
from bson.objectid import ObjectId
from forecast.class_func2 import func2
from forecast.class_func3 import func3
from forecast.class_func1 import func1
from flask import Blueprint, request, jsonify
from forecast.database import connect, disconnect, getDb
from bson.json_util import loads, dumps, CANONICAL_JSON_OPTIONS

forecastApi = Blueprint('forecastApi', __name__)
@forecastApi.route('/forecast', methods=['POST'])

def forecast():
	req = request.json
	phase = req['phase']
	well_id = req['well_id']
	datum_id = req['datum_id']
	para_dict = req['para_dict']
	f1 = func1()

	data_pipeline = [
		{'$match': {'well': ObjectId(well_id)}},
		{
			'$group': {
				'_id': '$well',
				'index': {'$push': '$index'},
				phase: {'$push': {'$divide': ['$'+phase, 30]}},
			}
		}
	]

	client = connect()
	db = getDb(client)

	try:
		data = list(db['well-months'].aggregate(data_pipeline))[0]
		np_idx = np.array(data['index'])
		idx_sort_arg = np.argsort(np_idx)
		sort_idx = np_idx[idx_sort_arg].tolist()
		sort_value = np.array(data[phase])[idx_sort_arg].tolist()

		fun1input = {
			'data': {
				'idx': sort_idx,
				'value': sort_value
			},
			'phase': phase,
			'well_id': well_id,
			'para_dict': para_dict
		}

		output = f1.body(fun1input)

		if output['goto'] == 2:
			out = fun2(output, well_id, db)
		else:	
			out = fun3(output)

		prop = 'data.'+well_id+'.'+phase
		db['forecast-datas'].update_one({'_id': ObjectId(datum_id)}, {'$set': {prop: out}})
		disconnect(client)
		return 'complete', 200

	except Exception as e:
		disconnect(client)
		pprint.pprint(str(e))
		return str(e), 400


def fun2(input, well_id, db):
	ana_result = input['ana_result']
	phase = input['phase']
	label = int(ana_result['label'])
	fit = ana_result.pop('fit')
	ana_result.update(fit)
	ana_result['well_id'] = input['well_id']
	ana_result['phase'] = input['phase']
	well_pipeline = [
		{'$match': {'_id': ObjectId(well_id)}},
		{
			'$project': {
				'_id': False,
				'MD': {'$toDouble': '$measured_depth'},
				'LL': {'$toDouble': '$lateral_length'},
				'Prop': {'$toDouble': '$total_proppant'},
				'TVD': {'$toDouble': '$true_vertical_depth'},
			}
		}
	]

	point_pipeline = [
		{'$match': {'well': ObjectId(well_id)}},
		{'$project': {
			'_id': False,
			'lat': {'$arrayElemAt': ['$location.coordinates', 1]},
			'long': {'$arrayElemAt': ['$location.coordinates', 0]},
		},
		},
	]

	well = list(db.wells.aggregate(well_pipeline))[0]
	point = list(db.points.aggregate(point_pipeline))[0]
	well.update(point)
	well.update(ana_result)
	group_info = {
		'label' : label,
		'phase' : phase,
		'table' : [well],
		'para_dict' : input['para_dict'],
		'model_download_str' : 'forecast/' + phase + '_' + str(label) + '.joblib'
	}
	f2 = func2()
	f2_ret = f2.body(group_info)
	
	return f2_ret[0]

def fun3(input):
	f3 = func3()
	output = f3.body(input)
	return output
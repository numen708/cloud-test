import numpy as np
import pandas as pd
from forecast.skeleton_Prob_v2 import get_prob
from forecast.segment_templates import segment_template
from forecast.RVG_arps import generate_D1_B2, generate_d_b
#### func3_class
class func3:
    def T1(self, well_phase):
        well_data = pd.DataFrame(well_phase['data']).fillna(0)
        analysis_data = np.array(well_data[['idx', 'value']])
        para_dict = well_phase['para_dict']
        ana_result = well_phase['ana_result']
        return {'ana_result': ana_result, 'data' : analysis_data, 'para_dict' : para_dict, 'well_id':well_phase['well_id'], 'phase':well_phase['phase']}
    
    def analysis(self, T1_out):
        seg = segment_template()
        data = T1_out['data']
        ana_result = T1_out['ana_result']
        t_peak = ana_result['t_peak']
        this_p = ana_result['fit']['p']
        this_p_fixed = ana_result['fit']['p_fixed']
        para_dict = T1_out['para_dict']
        model_name = para_dict['model_name']
        t_end_life = ana_result['t_end_life']
        np.random.seed(1)
        if model_name == 'segment_arps_4_wp':
            para_candidates = generate_D1_B2(5000, para_dict)
        else:
            para_candidates = generate_d_b(5000, para_dict)
        
        prob = get_prob()
        prob.set_seed(1)
        P_dict = prob.get_percentile(data, model_name, t_peak, this_p, this_p_fixed, para_candidates,t_end_life, para_dict)
        p2_seg_para = {
            't_first' : ana_result['t_first'],
            't_end_data' : ana_result['t_end_data'],
            't_end_life' : ana_result['t_end_life'],
            'q_final' : para_dict['q_final'],
            'D_lim_eff' : para_dict['D_lim_eff']
        }
        
        if ana_result['first_peak']:
            plot_idx = ana_result['t_first_valid_data']
        else:
            plot_idx = ana_result['t_peak']
        
        for k,v in P_dict.items():
            this_segs = seg.p2seg_s[model_name](v['p'], v['p_fixed'], p2_seg_para)
            ret_segs = []
            for this_seg in this_segs:
                if this_seg['end_idx'] >= plot_idx:
                    ret_segs += [this_seg]
            P_dict[k] = ret_segs
            
        p_extra = {'plot_idx' : plot_idx, 'well_id' : T1_out['well_id'], 'phase' : T1_out['phase']}
        return {'P_dict':P_dict, 'p_extra':p_extra}
    
    def T2(self, ana_out):
        P_dict = ana_out['P_dict']
        for k,v in P_dict.items():
            for elem in v:
                for kk,vv in elem.items():
                    if type(vv)!=str: 
                        elem[kk] = float(vv)
        
        p_extra = ana_out['p_extra']
        p_extra['plot_idx'] = float(p_extra['plot_idx'])
            
        return ana_out

    def body(self, well_phase):
        T1_output = self.T1(well_phase)
        ana_result = self.analysis(T1_output)
        return self.T2(ana_result)
        
import copy
import numpy as np
import pandas as pd
from forecast.class_p_eur import p_eur
from forecast.skeleton_DCA_v3 import get_DCA
from forecast.plugin_prob import keep_ratio_s
from scipy.optimize import differential_evolution

class get_prob:
    def __init__(self):
        self.keep_ratio_s = keep_ratio_s
        self.random_seed = None
        
    def para_parse(self, p, p_fixed, para_candidates, model_name):
        det = get_DCA()
        p_name = det.model_p_name[model_name]
        p_fixed_name = det.model_p_fixed_name[model_name]
        para_table = pd.DataFrame([np.concatenate([p,p_fixed])] * para_candidates.shape[0], 
                                   columns = p_name + p_fixed_name)
        
        for name in para_candidates.columns:
            para_table[name] = para_candidates[name]
        
        p_table = np.array(para_table[p_name])
        p_fixed_table = np.array(para_table[p_fixed_name])
        return p_table, p_fixed_table
    
    def para_filter(self, data_transformed, p_table, p_fixed_table, pred_func, best_fit_pred, loss_type = 'ra', keep_ratio = 0.2):
        #### first screen
        keep_limit = int(keep_ratio * p_table.shape[0])
        my_estimator = get_DCA()
        loss = np.zeros(p_table.shape[0])
    
        for i in range(p_table.shape[0]):
            this_pred = pred_func(data_transformed[:,0],p_table[i,:],p_fixed_table[i,:])
            loss[i] = my_estimator.errorfunc_s[loss_type](data_transformed[:,1], this_pred)
            
        ind_ret = np.argsort(loss)[:keep_limit]
        return p_table[ind_ret,:], p_fixed_table[ind_ret,:]
    
    def get_percentile(self, data, model_name, t_peak, p_best, p_fixed_best, para_candidates, well_life_idx, para_dict):
        # print(para_candidates.min())
        # print(para_candidates.max())
        ###
        percentiles = para_dict['percentile']
        q_final = para_dict['q_final']
        D_lim_eff = para_dict['D_lim_eff']
        #####3
        det = get_DCA()
        eur_cal = p_eur()
        
        p_table, p_fixed_table = self.para_parse(p_best,p_fixed_best,para_candidates, model_name)
        transformed_data = det.transform_s['afterpeak'](data, t_peak)
        # plt.figure()
        # plt.axvline(t_peak)
        # plt.scatter(transformed_data[:,0], transformed_data[:,1])
        pred_func = det.predict_s[model_name]
        best_fit_pred = pred_func(transformed_data[:,0], p_best, p_fixed_best)
        keep_ratio = para_dict['dispersion'] * self.keep_ratio_s[model_name](transformed_data, best_fit_pred, p_best, p_fixed_best)
        p_table, p_fixed_table = self.para_filter(transformed_data, p_table, p_fixed_table, pred_func, best_fit_pred, keep_ratio=keep_ratio)
        t_pred = np.arange(t_peak, well_life_idx, 30)
        q_pred = eur_cal.segment_arps_4_wp_pred(t_pred,p_table, p_fixed_table, q_final, D_lim_eff, data[-1,0], well_life_idx)
        data_table = np.array([data[:,1]]*p_table.shape[0])
        #########
        mix_table = np.concatenate([data_table, q_pred[:, t_pred > data[-1, 0]]], axis=1)
        # plt_x = t_pred[:100]
        # y1 = np.min(q_pred[:,:100], axis=0)
        # y2 = np.max(q_pred[:,:100], axis=0)
        # plt.scatter(data[:,0],data[:,1])
        # plt.fill_between(plt_x, y1, y2, alpha = 0.5)
        percentiles_arr = np.arange(1,100,1)
        percentile_table = np.apply_along_axis(lambda x: np.percentile(x,percentiles_arr), 0, mix_table)
        eur_list = np.sum(mix_table, axis =1)
        eur_perc = np.sum(percentile_table, axis =1)
        target_eurs = np.percentile(eur_list, percentiles)
        target_eur_dif = np.abs(eur_perc.reshape(-1,1) - target_eurs.reshape(1,-1))
        ### 
        target_time_perc_ind = []
        for i in range(len(percentiles)):
            if percentiles[i] < 50:
                search_range = (percentiles_arr>=percentiles[i])
            elif percentiles[i] > 50:
                search_range = (percentiles_arr<=percentiles[i])
            else:
                search_range = (percentiles_arr<=52) & (percentiles_arr>=48)
            
            search_arr = target_eur_dif[search_range, i]
            search_inds = percentiles_arr[search_range]
            this_ind = search_inds[np.argmin(search_arr)]
            target_time_perc_ind += [this_ind]
        
        target_time_perc_ind = np.array(target_time_perc_ind)
        target_time_perc = percentiles_arr[target_time_perc_ind]
        ###############################
        fit_len = np.min([int(2*(data[-1,0] - t_peak)), well_life_idx - t_peak])
        fit_data = q_pred[:, t_pred <= (t_peak+fit_len)]
        fit_t = t_pred[t_pred <= (t_peak+fit_len)]
        ret_dict = {}
        for i in range(len(percentiles)):
            perc = percentiles[i]
            this_name = 'P' + str(perc)
            this_p_data = np.stack([fit_t, np.apply_along_axis(lambda x: np.percentile(x, target_time_perc[i]), 0, fit_data)], axis = 1)
            this_x, para_fit = self.get_para(this_p_data, p_best, p_fixed_best, para_candidates, model_name, para_dict)
            this_p = copy.deepcopy(p_best)
            this_p_fixed = copy.deepcopy(p_fixed_best)
            for i in range(len(para_fit)):
                this_p[para_fit[i]['ind']] = this_x[i]
            
            ret_dict[this_name] = {'p' : this_p, 'p_fixed': this_p_fixed}
        
        ret_dict['best'] = {'p':p_best, 'p_fixed':p_fixed_best}

        return ret_dict
    
    def get_para(self, data, p_best, p_fixed_best, para_candidates, model_name, para_dict):
        det_1 = get_DCA()
        model_p_name = det_1.model_p_name
        para_names = para_candidates.columns
        para_fit = []
        this_range = []
        for name in para_names:
            for i in range(len(model_p_name[model_name])):
                comp_name = model_p_name[model_name][i]
                if comp_name == name:
                    para_fit += [{'name':name, 'ind' : i}]
                    this_range += [tuple(para_dict[name])]
                    break
        
        args = (data, p_best, p_fixed_best, para_fit, model_name)
        result = differential_evolution(self.alpha, args = args, bounds = this_range, seed = self.random_seed)
        return result.x, para_fit
        
    def alpha(self, x, *args):
        data, p_best, p_fixed_best, para_fit, model_name = args
        det = get_DCA()
        this_p = copy.deepcopy(p_best)
        this_p_fixed = copy.deepcopy(p_fixed_best)
        for i in range(len(para_fit)):
            this_p[para_fit[i]['ind']] = x[i]
        
        pred = det.predict_s[model_name](data[:,0], this_p, this_p_fixed)
        err = np.mean(np.abs(pred - data[:,1]))
        return err
    
    def set_seed(self, random_seed):
        self.random_seed = random_seed
        
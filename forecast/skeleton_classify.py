import numpy as np
from sklearn.metrics import r2_score
from forecast.skeleton_DCA_v3 import get_DCA
from scipy.optimize import differential_evolution
from forecast.funcs_xuyan import find_peak_valley_vec
##########################################################
def classify_default(transformed_data, peak_dict, last):
    PEC = peak_dict['PEC']
    peak_t = peak_dict['peak_t']
    
    # peak_q = peak_dict['q_peak']
    def DAP_thres(DAP): ## 4:0.64, 5: 0.56, 6: 0.48, 6:0.40
        return 0.96 - 0.08*DAP
    para_dict = {'CFD':[0,2], 'DAP':[3,7], 'thres':DAP_thres}
        
    CFD = transformed_data.shape[0]
    DAP = np.sum(transformed_data[:,0] >= peak_t)
    Thres = para_dict['thres'](DAP)
    if last:
        if CFD <= para_dict['CFD'][0]:
            return 1
        elif CFD <=para_dict['CFD'][1]:
            return 2
        else:
            data_ap = transformed_data[transformed_data[:,0]>=peak_t,:]
            def alpha(p,*args):
                d = p
                data_ap, q =args
                t = data_ap[:,0]
                y_true = data_ap[:,1]
                pred = q*np.exp(d*(t-t[0]))
                return np.mean(np.square(y_true-pred))
            ars = data_ap,data_ap[0,1]
            result = differential_evolution(alpha, args = ars, bounds = [(-50,0)],popsize = 15)
            pred_ap = data_ap[0,1] * np.exp((data_ap[:,0]-peak_t) * result.x[0])
            
            r2 = r2_score(data_ap[:,1],pred_ap)
            if DAP<=para_dict['DAP'][0]:
                if PEC == 1:
                    return 3
                else:
                    return 5
            elif DAP <= para_dict['DAP'][1]:
                if r2 > Thres:
                    if PEC == 1:
                        return 7
                    else:
                        return 9
                else:
                    if PEC == 1:
                        return 4
                    else:
                        return 6
            else:
                if PEC == 1:
                    return 8
                else:
                    return 10
    else:
        if CFD <= para_dict['CFD'][0]:
            return 1
        elif CFD <=para_dict['CFD'][1]:
            return 2
        else:
            data_ap = transformed_data[transformed_data[:,0]>=peak_t,:]
            def alpha(p,*args):
                d = p
                
                data_ap, q =args
                t = data_ap[:,0]
                y_true = data_ap[:,1]
                pred = q*np.exp(d*(t-t[0]))
                return np.mean(np.square(y_true-pred))
            ars = data_ap,data_ap[0,1]
            result = differential_evolution(alpha, args = ars, bounds = [(-50,0)],popsize = 15)
            pred_ap = data_ap[0,1] * np.exp((data_ap[:,0]-peak_t) * result.x[0])
            
            r2 = r2_score(data_ap[:,1],pred_ap)
            if DAP<=para_dict['DAP'][0]:
                return 3
            elif DAP<=para_dict['DAP'][1]:
                if r2 > Thres:
                    return 7
                else:
                    return 4
            else:
                return 8

classify_s = {'default':classify_default}


#### peak_vec
def get_peak_vec(transformed_data, peak_para_dict=None):
    para_dict = {'T1':3, 'T2':0.3, 'last':1}
    if peak_para_dict != None:
        for k in list(peak_para_dict.keys()):
            para_dict[k] = peak_para_dict[k]
    
    t = transformed_data[:,0]
    rate = transformed_data[:,1]
    if len(t)==0:
        return {'PEC':1,  'peak_t':-1, 'q_peak':0, 'first_peak':1}

    p, v = find_peak_valley_vec(t, rate,para_dict['T1'], para_dict['T2'])

    if len(p) == 1:
        t1 = t[p[0]]
        q1 = rate[p[0]]
        return {'PEC':1,  'peak_t':t1, 'q_peak':q1, 'first_peak':1}
    
    elif len(p) > 1:
        p_values = [rate[i] for i in p]
        if p_values[-1] == max(p_values):
            p_ind =  p[-1]
            return {'PEC':1, 'peak_t': t[p_ind], 'q_peak': rate[p_ind], 'first_peak':0}
        else:
            if para_dict['last'] == 1:
                p_ind  = p[-1]
                this_first = 0
            else:
                p_ind = p[np.argmax(p_values)]
                if p_ind == p[0]:
                    this_first = 1
                else:
                    this_first = 0
                
            return {'PEC':2,  'peak_t': t[p_ind], 'q_peak': rate[p_ind], 'first_peak':this_first}
    

find_peak_s = {'default':get_peak_vec}
########################################################
class classification:
    def __init__(self):
        self.find_peak_s = find_peak_s
        self.classify_s = classify_s
        
    def classify(self, data, para_dict):
        peak_method = 'default'
        classify_method = 'default'
        last = 1
        det_1 = get_DCA()
        transformed_data = det_1.transform_s['default'](data,1)
        peak_dict = self.find_peak_s[peak_method](transformed_data) ### the user should select which peak to use if we have multiple peaks 
        label = self.classify_s[classify_method](transformed_data, peak_dict, last)
        c1 = (data.shape[0]<=1)
        if not c1:
            c1 = c1 | (data[-1,0] - data[0,0] <= para_dict['ML_data_thres'])
        
        if c1 & (label >=7):
            label -= 4
        
        return peak_dict['peak_t'], label, peak_dict['first_peak']
        
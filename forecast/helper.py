import numpy as np
from copy import deepcopy
from forecast.plugin import model_p_name, model_p_fixed_name
######### common_number
common_number = {'days_in_year':365.25}


### helper
def arps_sw(q_start, b, D, D_lim_eff, idx_start, idx_end_data, life_idx):  ### idx_sw is integer alwasy 
    days_in_year = common_number['days_in_year']
    idx_last = np.max([idx_end_data, idx_start])
    D_last = D/(1+b*D*(idx_last - idx_start))
    D_lim = (np.power(1-D_lim_eff, -b) - 1) / days_in_year/ b
    if D_last > D_lim:
        idx_sw = (D/D_lim - 1)/(b * D) + idx_start
    else:
        idx_sw = idx_last

    D_sw = D/(1 + b * D * (idx_sw - idx_start))
    D_exp = D_sw
    D_exp_eff = 1 - np.exp(-days_in_year * D_exp)
    return idx_sw, D_exp, D_exp_eff

class converts:
    def create_para_hash(self, model_name, para_name):
        p_name = model_p_name[model_name]
        p_fixed_name = model_p_fixed_name[model_name]
        para_hash = {}
        for name in para_name:
            this_hash = {}
            this_hash['goto_p'] = (name in p_name)
            if this_hash['goto_p']:
                this_hash['ind'] = np.argwhere(np.array(p_name) == name)[0,0]
            else:
                this_hash['ind'] = np.argwhere(np.array(p_fixed_name) == name)[0,0]
            para_hash[name] = this_hash  
        
        return para_hash
    
    def hirchy_dict2flat_dict(self, P_dict, model_name, percentiles, para_name):
        ret_dict = {}
        p_name = model_p_name[model_name]
        p_fixed_name = model_p_fixed_name[model_name]
        para_hash = self.create_para_hash(model_name, para_name)
        for i in range(len(p_name)):
            this_name = 'best_' + p_name[i]
            ret_dict[this_name] = P_dict['best']['p'][i]
            
        for i in range(len(p_fixed_name)):
            this_name = 'best_' + p_fixed_name[i]
            ret_dict[this_name] = P_dict['best']['p_fixed'][i]
            
        for perc in percentiles:
            p_name = 'P' + str(perc)
            for name in para_name:
                this_name = p_name + '_' + name
                if para_hash[name]['goto_p']:
                    ret_dict[this_name] = P_dict[p_name]['p'][para_hash[name]['ind']]
                else:
                    ret_dict[this_name] = P_dict[p_name]['p_fixed'][para_hash[name]['ind']]
                    
        return ret_dict
    
    def flat_dict2hirchy_dict(self, flat_dict, model_name, percentiles, para_name):
        ret_dict = {}
        p_name = model_p_name[model_name]
        p_fixed_name = model_p_fixed_name[model_name]
        para_hash = self.create_para_hash(model_name, para_name)
        best_p = []
        for name in p_name:
            if name in para_name:
                best_p += [0]
            else:
                best_p += [flat_dict['best_'+name]]
        best_p = np.array(best_p, dtype = float)
        
        best_p_fixed =[]
        for name in p_fixed_name:
            if name in para_name:
                best_p_fixed += [0]
            else:
                best_p_fixed += [flat_dict['best_'+name]]
        best_p_fixed = np.array(best_p_fixed, float)

        for perc in percentiles:
            p_name = 'P' + str(perc)
            this_p = deepcopy(best_p)
            this_p_fixed = deepcopy(best_p_fixed)
            for name in para_name:
                this_name = p_name + '_' + name
                if para_hash[name]['goto_p']:
                    this_p[para_hash[name]['ind']] = flat_dict[this_name]
                else:
                    this_p_fixed[para_hash[name]['ind']] = flat_dict[this_name]
                
            ret_dict[p_name] = {'p':np.array(this_p), 'p_fixed': np.array(this_p_fixed)}
        
        return ret_dict
import numpy as np
from scipy.optimize import differential_evolution
from forecast.plugin import func_s, prepare_s, predict_s, errorfunc_s, transform_s, default_transform_type, model_p_name, model_p_fixed_name

class get_DCA:
    def __init__(self):
        self.func_s = func_s
        self.prepare_s = prepare_s
        self.predict_s = predict_s
        self.errorfunc_s = errorfunc_s
        self.transform_s = transform_s
        self.default_transform_type = default_transform_type
        self.error_type = 'ra'
        self.pop_size = 15
        self.model_p_name = model_p_name
        self.model_p_fixed_name = model_p_fixed_name
        self.random_seed = 1
        
    def set_seed(self, seed):
        self.random_seed = seed
        
    def set_errortype(self,errortype):
        self.error_type = errortype
    
    def set_pop_size(self,pop_size):
        self.pop_size = pop_size
        
    def get_params(self, data,  t_peak, label, model_name, ranges = None):
        ### the data is the original data, transformation inside will take care of the
        ### data cleaning
        ### model_name must be specified
        ### this function only works for single model_name, without explicitly given transform type and ranges, we will get them from defaults and data
        
        func = self.func_s[model_name]
        prepare = self.prepare_s[model_name]
        
        transform_type = self.default_transform_type[model_name]
            
        transformer = self.transform_s[transform_type]
        fixed_parameters, p_range, transformed_data = prepare(data, t_peak, transformer)
        ###print(fixed_parameters)
        if ranges!=None:
            for i in range(len(ranges['ind'])):
                p_range[ranges['ind'][i]] = ranges['range'][i]
            
        arg = (func, self.errorfunc_s[self.error_type], transformed_data, fixed_parameters)
        result = differential_evolution(self.alpha, args=arg, bounds=p_range, popsize=self.pop_size, seed=self.random_seed)
        
        return model_name, result.x, result.fun, fixed_parameters
    
    def alpha(self,p,*args):
        func, err_func, transformed_data, fixed_p = args
        pred = func(transformed_data[:,0], p, fixed_p)
        loss = err_func(transformed_data[:,1], pred)
        return loss
    
    def get_range(self,model_name, para_dict, label):
        ret_ind = []
        ret_range = []
        name_list = np.array(self.model_p_name[model_name])
        for name in list(para_dict.keys()):
            ind_list = np.argwhere(name_list==name)
            if(len(ind_list)==0):
                continue
            else:
                ret_ind += [ind_list[0,0]]
                ret_range += [tuple(para_dict[name])]
        
        ret_dict = {'ind':ret_ind, 'range':ret_range}
        return ret_dict

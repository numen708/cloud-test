import datetime
import numpy as np
import pandas as pd
from flask import jsonify
from forecast.skeleton_DCA_v3 import get_DCA
from forecast.skeleton_no_data import get_no_data
from forecast.skeleton_classify import classification

class func1:
    def T1(self, well_phase):
        well_data = pd.DataFrame(well_phase['data']).fillna(0)
        analysis_data = np.array(well_data[['idx', 'value']])
        para_dict = well_phase['para_dict']
        
        return {'para_dict': para_dict, 'analysis_data' : analysis_data}
    
    def analysis(self, T1_out):
        classifier = classification()
        det = get_DCA()
        det.set_seed(1)
        no_data = get_no_data()



        para_dict = T1_out['para_dict']
        model_name = para_dict['model_name']
        data = T1_out['analysis_data']
        t_peak, label, first_peak = classifier.classify(data, para_dict)
        clean_data = det.transform_s[det.default_transform_type[model_name]](data, t_peak)

        if data.shape[0] >0:
            t_first = data[0, 0]
            t_end_data = data[-1,0]
            if clean_data.shape[0] >0:
                t_first_valid_data = clean_data[0, 0]
            else:
                t_first_valid_data = t_first
            
        else:
            today = datetime.date.today()
            t_first = (today - datetime.date(1900,1,1)).days
            t_end_data = t_first
            t_first_valid_data = t_first

        
        ret = {}
        ret['t_peak'] = t_peak
        ret['best_t_first'] = t_first
        ret['t_first']   = t_first
        ret['label'] = label
        ret['t_end_data'] = t_end_data
        ret['first_peak'] = first_peak
        ret['t_first_valid_data'] = t_first_valid_data
        first_date = datetime.date(1900, 1, 1) + datetime.timedelta(int(t_first))
        first_year = first_date.year
        first_month = first_date.month
        first_day = first_date.day
        end_year = first_year + para_dict['well_life']
        end_date = datetime.date(end_year, first_month, first_day)
        t_end_life = (end_date - datetime.date(1900, 1, 1)).days
        ret['t_end_life'] = t_end_life

        if label > 6:
            update_range = det.get_range(model_name, para_dict, label)
            this_model, this_p, loss, this_p_fixed = det.get_params(data,  t_peak, label, model_name = model_name , ranges = update_range)
            ret['fit'] = {
                    'p' : this_p,
                    'p_fixed': this_p_fixed
                    }
        else:
            nodata_range = no_data.get_range(model_name, para_dict, label)
            ret['fit'] = no_data.get_paras(data, t_peak, label, model_name, nodata_range)
        
        return ret
    
    def T2(self, analysis_out):
        ### index
        analysis_out['best_t_first'] = float(analysis_out['best_t_first'])
        analysis_out['t_first'] = float(analysis_out['t_first'])
        analysis_out['t_first_valid_data'] = float(analysis_out['t_first_valid_data'])
        analysis_out['label'] = float(analysis_out['label'])
        analysis_out['t_end_data'] = float(analysis_out['t_end_data'])
        analysis_out['first_peak'] = float(analysis_out['first_peak'])
        analysis_out['t_peak'] = float(analysis_out['t_peak'])
        analysis_out['t_end_life'] = float(analysis_out['t_end_life'])
        if analysis_out['label'] <=6:
            for k, v in analysis_out['fit'].items():
                analysis_out['fit'][k] = float(v)
        else:
            for k, v in analysis_out['fit'].items():
                analysis_out['fit'][k] = list(map(float, v))
                
        return analysis_out
    
    def body(self, well_phase):
        T1_out = self.T1(well_phase)
        ana_out = self.analysis(T1_out)
        T2_out = self.T2(ana_out)
        well_phase['ana_result'] = T2_out
        if ana_out['label'] <=6:
            well_phase['goto'] = 2
            well_phase.pop('data')
        else:
            well_phase['goto'] = 3    
        return well_phase
    
    
   
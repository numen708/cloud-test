import datetime
import numpy as np
import pandas as pd
from copy import deepcopy
from forecast.skeleton_DCA_v3 import get_DCA
from forecast.segment_templates import segment_template
from scipy.optimize import differential_evolution
from forecast.plugin import model_p_name, model_p_fixed_name

class c4:
    def T1(self, c4_input):
        ret = deepcopy(c4_input)
        well_list = ret['well_list']
        for i, this_dict in enumerate(well_list):
            this_data = np.array(pd.DataFrame(this_dict['data'])[['idx','value']].sort_values('idx'))
            this_dict['cum_value'] = this_dict['data']['cum_value']
            this_dict['data'] = np.array(this_data)
        
        if 'plot_dict' not in list(ret.keys()):
            ret['plot_dict'] = {
                'high_months': 72,
                'before_peak_months': 12,
                'high_freq': 1,
                'low_freq' : 36
            }
        return ret
    
    def analysis(self, T1_ret):
        ####### input parsing
        para_dict = T1_ret['para_dict']
        fit_para = T1_ret['fit_para']
        well_life = para_dict['well_life']
        num_month = well_life * 12
        well_list = T1_ret['well_list']
        use_p = fit_para['use_percentile']
        plot_dict = T1_ret['plot_dict']
        ################################ initialization
        seg = segment_template()
        noalign_data_mat = np.zeros((len(well_list), well_life * 12))
        noalign_data_mat[:] = np.nan
        noalign_idx_mat = np.zeros(noalign_data_mat.shape)
        eur_vec = np.zeros(len(well_list))
        str_date_19000101 = '1900-01-01'
        date64_19000101 = np.datetime64(str_date_19000101)
        noalign_datamat_idx = np.arange(num_month, dtype = int)
        ################################ calculate noalign_data_mat and eur_vec
        for i, this_dict in enumerate(well_list):
            #### parse the input of each well
            this_forecast_para = this_dict['P_dict'][use_p]
            this_data = this_dict['data']
            this_start_idx = this_data[0,0]

            ########## genearte predictions
            this_start_date = date64_19000101 + int(this_start_idx)
            this_start_date_M = this_start_date.astype('datetime64[M]')
            this_month_arr = np.array([this_start_date_M] * num_month) + noalign_datamat_idx
            this_t_idx = (this_month_arr.astype('datetime64[D]')+14 - date64_19000101).astype(int)
            this_row = seg.predict(this_t_idx, this_forecast_para)
            noalign_idx_mat[i,:] = this_t_idx
            #### put production data into it
            ###### group monthly production together
            data_date = np.array([date64_19000101] * this_data.shape[0]) + this_data[:,0].astype(int)
            data_month = data_date.astype('datetime64[M]').astype(int)%12 + 1
            data_year = data_date.astype('datetime64[Y]').astype(int) + 1970
            data_df = pd.DataFrame(np.column_stack([this_data, data_year, data_month]), columns = ['idx','value','year','month'])
            group_df = data_df.groupby(['year','month']).mean()
            ####### insert the month production
            group_ym = np.array(list(map(list,group_df.index.values)))
            group_month = group_ym[:,1]
            group_year = group_ym[:,0]
            start_year = group_year[0]
            start_month = group_month[0]
            group_mat_idx = ((group_month - start_month) + (group_year - start_year) * 12).astype(int)
            this_row[group_mat_idx] = np.array(group_df['value'])
            noalign_data_mat[i,:] = this_row
            ###### genearate eur
            end_life_date_str = str(int(this_start_date.astype(str)[:4]) + well_life) + this_start_date.astype(str)[4:]
            end_life_date = np.datetime64(end_life_date_str)
            t_end_life = (end_life_date - date64_19000101).astype(int)
            
            cum_data = this_dict['cum_value'] ### cum data is not the one we need 
            end_data_idx = this_data[-1,0]
            this_eur = seg.eur(cum_data, end_data_idx, this_start_idx, t_end_life, this_dict['P_dict'][use_p])
            eur_vec[i] = this_eur
        
        ################################ calculate the align_data_mat
        peak_ind = np.apply_along_axis(np.argmax, 1, noalign_data_mat)
        max_peak_ind = np.max(peak_ind)
        align_data_mat = np.zeros((noalign_data_mat.shape[0], noalign_data_mat.shape[1] + max_peak_ind))
        align_data_mat[:] = np.nan
        align_idx_mat = np.zeros(align_data_mat.shape)
        for i in range(len(well_list)):
            this_peak = peak_ind[i]
            this_ind = noalign_datamat_idx + (max_peak_ind - this_peak)
            align_data_mat[i,this_ind] = noalign_data_mat[i,:]
            align_idx_mat[i,this_ind] = noalign_idx_mat[i,:]
            
            left_date = date64_19000101 + int(noalign_idx_mat[i,0])
            left_arr_len = (max_peak_ind - this_peak)
            left_idx_arr = np.arange(-left_arr_len, 0, 1, dtype = int) +  left_date.astype('datetime64[M]')
            left_idx_arr = (left_idx_arr.astype('datetime64[D]') + 14 - date64_19000101).astype(int)
            align_idx_mat[i,:left_arr_len] = left_idx_arr
            
            right_date = date64_19000101 + int(noalign_idx_mat[i,-1])
            right_arr_len = this_peak
            right_idx_arr = np.arange(1, right_arr_len + 1, dtype = int) +  right_date.astype('datetime64[M]')
            right_idx_arr = (right_idx_arr.astype('datetime64[D]') + 14 - date64_19000101).astype(int)
            align_idx_mat[i,(align_idx_mat.shape[1] - right_arr_len):] = right_idx_arr
        
        cum_dict = self.get_cum({'data': noalign_data_mat, 'idx':noalign_idx_mat})
        cum_dict['cum_pred_t_vec'] = noalign_idx_mat[0,:] - noalign_idx_mat[0,0]
        ####  peaks
        noalign_p80_value = np.nanpercentile(noalign_data_mat, 80, axis=0)
        noalign_peak_ind = np.argmax(noalign_p80_value)
        
        align_peak_ind = np.nanargmax(align_data_mat[0,:])
        ret = {
                'align' : {'data' : align_data_mat, 'idx' : align_idx_mat, 'peak_ind' : align_peak_ind},
                'noalign' : {'data': noalign_data_mat, 'idx':noalign_idx_mat, 'peak_ind' : noalign_peak_ind},
                'eur_dict' : {'eur' : eur_vec, 'percentile' : eur_vec.argsort().argsort()/eur_vec.shape[0], 
                              'symbol_eur' : np.percentile(eur_vec,[10, 50, 90]), 'symbol_name':[10,50,90]},
                'cum_dict' : cum_dict
              }
        ################# genearete default user_input:
        default_input = {
                'slice_range' : {
                        'noalign':[0,noalign_data_mat.shape[1]],
                        'align': [int(0.8 * max_peak_ind), align_data_mat.shape[1]]
                        },
                'compare_range': [0, cum_dict['cum'].shape[0]],
                'TC_model' : 'segment_arps_4_wp',
                'TC_percentile' : [10, 50 ,90]
                }
        ret['default_input'] = default_input
        ### generating data for plot
        high_months = plot_dict['high_months']
        before_peak_months = plot_dict['before_peak_months']
        high_freq = plot_dict['high_freq']
        low_freq = plot_dict['low_freq']

        noalign_x = np.arange(0, high_months, high_freq, dtype = int)
        noalign_x = np.concatenate([noalign_x, np.arange(high_months, num_month, low_freq, dtype=int)])
        if noalign_peak_ind not in noalign_x:
            noalign_x = np.append(noalign_x, noalign_peak_ind)
            noalign_x = np.sort(noalign_x)
        #
        align_3_left_idx = np.max([0, max_peak_ind - before_peak_months])
        align_3_right_idx = align_3_left_idx + high_months
        align_3_x = np.arange(align_3_left_idx, align_3_right_idx, high_freq, dtype = int)
        
        align_left_36_left_idx = 0
        align_left_36_x = np.arange(align_left_36_left_idx, align_3_left_idx, low_freq, dtype = int)
        
        align_right_36_right_idx = num_month
        align_right_36_x = np.arange(align_3_right_idx, align_right_36_right_idx, low_freq, dtype = int)
        align_x = np.concatenate([align_left_36_x, align_3_x, align_right_36_x])
        if align_peak_ind not in align_x:
            align_x = np.append(align_x, align_peak_ind)
            align_x = np.sort(align_x)
        
        align_earliest_idx = np.argwhere(np.invert(np.isnan(align_data_mat[:,0])))[0,0]
        #
        ret['plot_use'] = {
                'noalign':{'data':noalign_data_mat[:,noalign_x], 'x':noalign_x, 'early_idx' : 0},
                'align' : {'data':align_data_mat[:,align_x], 'x':align_x, 'early_idx' : align_earliest_idx}
                }
        return ret
    
    def get_cum(self, noalign_dict):
        idx_mat = noalign_dict['idx']
        data_mat = noalign_dict['data']
        min_idx = np.min(idx_mat)
        max_idx = np.max(idx_mat)
        min_month = (np.datetime64('1900-01-01') + int(min_idx)).astype('datetime64[M]')
        max_month = (np.datetime64('1900-01-01') + int(max_idx)).astype('datetime64[M]')
        month_arr = np.arange(min_month, max_month+1)
        nc = month_arr.shape[0]
        nr = idx_mat.shape[0]
        sum_mat = np.zeros((nr,nc))
        cum_subind = np.zeros((nr, 2), dtype = int)
        cum_vecind = np.zeros(nr*nc, dtype = bool)
        for i in range(nr):
            this_left_idx = ((np.datetime64('1900-01-01') + int(idx_mat[i,0])).astype('datetime64[M]') - month_arr[0]).astype(int)
            this_right_idx = ((np.datetime64('1900-01-01') + int(idx_mat[i,-1])).astype('datetime64[M]') - month_arr[0]).astype(int) + 1
            cum_subind[i,:] = np.array([this_left_idx, this_right_idx])
            cum_vecind[(this_left_idx + i*nc):(this_right_idx + i*nc)] = True
            sum_mat[i, this_left_idx:this_right_idx] = data_mat[i,:]
        
        sum_vec = np.nansum(sum_mat,axis=0)
        cum_vec = np.nancumsum(sum_vec)
        time_arr = ((month_arr.astype('datetime64[D]') + 14) - np.datetime64('1900-01-01')).astype(int)
        return {'idx': time_arr, 'cum': cum_vec, 'cum_subind' : cum_subind, 'cum_vecind' : cum_vecind}
    
    def T2(self, ana_out):
        ana_out['align']['data'] = np.where(np.isnan(ana_out['align']['data']), None, ana_out['align']['data'])
        ana_out['align']['data'] = ana_out['align']['data'].tolist()
        ana_out['align']['idx'] = ana_out['align']['idx'].tolist()
        ana_out['align']['peak_ind'] = int(ana_out['align']['peak_ind'])
        ana_out['noalign']['data'] = ana_out['noalign']['data'].tolist()
        ana_out['noalign']['idx'] = ana_out['noalign']['idx'].tolist()
        ana_out['noalign']['peak_ind'] = int(ana_out['noalign']['peak_ind'])
        
        ana_out['eur_dict']['eur'] = ana_out['eur_dict']['eur'].tolist()
        ana_out['eur_dict']['percentile'] = ana_out['eur_dict']['percentile'].tolist()
        ana_out['eur_dict']['symbol_eur'] = ana_out['eur_dict']['symbol_eur'].tolist()
    
        ana_out['cum_dict']['idx'] = ana_out['cum_dict']['idx'].tolist()
        ana_out['cum_dict']['cum'] = ana_out['cum_dict']['cum'].tolist()
        ana_out['cum_dict']['cum_subind'] = ana_out['cum_dict']['cum_subind'].tolist()
        ana_out['cum_dict']['cum_pred_t_vec'] = ana_out['cum_dict']['cum_pred_t_vec'].tolist()
        ana_out['cum_dict']['cum_vecind'] = ana_out['cum_dict']['cum_vecind'].tolist()
        ###########################
        
        ana_out['plot_use']['align']['data'] = np.where(np.isnan(ana_out['plot_use']['align']['data']), None, ana_out['plot_use']['align']['data'])
        ana_out['plot_use']['align']['data'] = ana_out['plot_use']['align']['data'].tolist()
        ana_out['plot_use']['align']['x'] = ana_out['plot_use']['align']['x'].tolist()
        ana_out['plot_use']['align']['early_idx'] = int(ana_out['plot_use']['align']['early_idx'])

        ana_out['plot_use']['noalign']['data'] = ana_out['plot_use']['noalign']['data'].tolist()
        ana_out['plot_use']['noalign']['x'] = ana_out['plot_use']['noalign']['x'].tolist()
        ###############################
        ana_out['default_input']['slice_range']['align'] = list(map(int, ana_out['default_input']['slice_range']['align']))
        ana_out['default_input']['slice_range']['noalign'] = list(map(int, ana_out['default_input']['slice_range']['noalign']))
        ana_out['default_input']['compare_range'] = list(map(int, ana_out['default_input']['compare_range']))
        ana_out['default_input']['TC_percentile'] = list(map(int, ana_out['default_input']['TC_percentile']))
        
        return ana_out
    
    def body(self, c4_input):
        return self.T2(self.analysis(self.T1(c4_input)))

##############################################
def generate_cum(segment, pred_t_vec, container, cum_subind):
    seg = segment_template()
    this_t = np.array(pred_t_vec)
    this_container = np.array(container)
    this_subind = np.array(cum_subind)
    pred = seg.predict(this_t, segment)
    single_cum = np.nancumsum(pred)
    for i in range(this_subind.shape[0]):
        this_container[this_subind[i,0]:this_subind[i,1]] += pred
        
    group_cum = np.nancumsum(this_container)
    return {'single_cum' : single_cum, 'group_cum':group_cum}


def segment_arps_4_wp_reach(p,p_fixed, target_eur, para_dict, reach_para = {'step': 3, 'lin_num':10}):
    c_seg = segment_template()
    start_idx = p_fixed[0]
    start_date = datetime.date(1900,1,1) + datetime.timedelta(int(start_idx))
    start_year = start_date.year
    end_year = start_year + para_dict['well_life']
    end_date = datetime.date(end_year, start_date.month, start_date.day)
    t_end_life = (end_date - datetime.date(1900,1,1)).days
    p2_seg_para = {
            't_first' : p_fixed[0],
            't_end_data' : p_fixed[0],
            't_end_life' : t_end_life,
            'q_final' : para_dict['q_final'],
            'D_lim_eff' : para_dict['D_lim_eff']
        }
    orig_segments = c_seg.p2seg_s['segment_arps_4_wp'](p,p_fixed, p2_seg_para)
    orig_eur = c_seg.eur(0,start_idx-10, start_idx, t_end_life, orig_segments)
    this_multiplier = target_eur/orig_eur
    this_range = [np.min([this_multiplier, 1]), np.max([this_multiplier, 1])]
    for i in range(reach_para['step']):
        this_candidates = np.linspace(this_range[0], this_range[1], reach_para['lin_num'])
        this_cal_eur = np.zeros(this_candidates.shape)
        for j in range(this_candidates.shape[0]):
            this_multiplier = this_candidates[j]
            this_p = deepcopy(p)
            this_p_fixed = deepcopy(p_fixed)
            this_p[0] = this_p[0] * this_multiplier
            this_p_fixed[3] = this_p_fixed[3] * this_multiplier
            this_segments = c_seg.p2seg_s['segment_arps_4_wp'](this_p, this_p_fixed, p2_seg_para)
            this_eur = c_seg.eur(0,start_idx-10, start_idx, t_end_life, this_segments)
            this_cal_eur[j] = this_eur
        
        this_eur_dif = np.abs(this_cal_eur - target_eur)
        next_cand = this_candidates[np.argsort(this_eur_dif)[0:2]]
        this_range = [np.min(next_cand), np.max(next_cand)]
    
    final_multiplier = next_cand[0]
    this_p = deepcopy(p)
    this_p_fixed = deepcopy(p_fixed)
    this_p[0] = this_p[0] * final_multiplier
    this_p_fixed[3] = this_p_fixed[3] * final_multiplier
    this_segments = c_seg.p2seg_s['segment_arps_4_wp'](this_p, this_p_fixed, p2_seg_para)
    this_eur = c_seg.eur(0,start_idx-10, start_idx, t_end_life, this_segments)
    return this_segments, this_eur

reach_eur_s = {'segment_arps_4_wp' : segment_arps_4_wp_reach}

def segment_arps_4_wp_build(benchmark_p, benchmark_p_fixed, buildup_dict):
    ##### only adjust time related parameter
    ##### q0 need to be recalculated becasue the time line changes

    ### calculate D0
    q_peak = benchmark_p_fixed[3]
    if benchmark_p_fixed[2] == 0:
        D0 = -1
    else:
        q0 = benchmark_p[0]
        D0 = - np.log(q_peak/q0) / benchmark_p_fixed[2]

    ##### update the time related parameter

    ret_p = deepcopy(benchmark_p)
    ret_p_fixed = deepcopy(benchmark_p_fixed)
    ret_p_fixed[1] = 0
    if buildup_dict['apply']:
        ret_p_fixed[2] = buildup_dict['days']
    else:
        ret_p_fixed[2] = benchmark_p_fixed[1] + benchmark_p_fixed[2]
    
    ##### calculate the new q0
    new_q0 = q_peak * np.exp(D0 * ret_p_fixed[2])
    ret_p[0] = new_q0

    return ret_p, ret_p_fixed

build_s = {'segment_arps_4_wp': segment_arps_4_wp_build}

def segment_arps_4_wp_adjust(adjusted_p, adjusted_p_fixed, adjusting_p, adjusting_p_fixed):
    ##### make sure t_first, t0, t_peak are the same as the target
    ##### adjust D0 related value(make sure the starting piece share the same slope)
    #####
    ret_p = np.array(adjusted_p)
    ret_p_fixed = np.array(adjusted_p_fixed)
    
    target_t_first = adjusting_p_fixed[0]
    target_minus_t0_t_first = adjusting_p_fixed[1]
    target_minus_t_peak_t0 = adjusting_p_fixed[2]
    
    ret_p_fixed[0] = target_t_first
    ret_p_fixed[1] = target_minus_t0_t_first
    ret_p_fixed[2] = target_minus_t_peak_t0
    #### calculate D0 and then apply, q0 is calcualted to make sure D0 are the same
    if target_minus_t_peak_t0 == 0:
        D0 = -1
    else:
        target_q0 = adjusting_p[0]
        target_q_peak = adjusting_p_fixed[3]
        D0 = -np.log(target_q_peak / target_q0) / target_minus_t_peak_t0
    
    this_q_peak = adjusted_p_fixed[3]
    this_q0 = this_q_peak * np.exp(D0 * target_minus_t_peak_t0)
    ret_p[0] = this_q0
    # print(ret_p,ret_p_fixed)
    return ret_p, ret_p_fixed

adjust_p_s = {'segment_arps_4_wp':segment_arps_4_wp_adjust}

class c7:
    def __init__(self):
        self.reach_eur_s = reach_eur_s
        self.build_s = build_s
        self.adjust_p_s = adjust_p_s
        

    def T1(self, c7_input):
        ret = {}
        fit_para = c7_input['fit_para']
        raw_data_mat = np.array(c7_input[fit_para['data']]['data'])
        raw_data_mat = np.where(raw_data_mat==None, np.nan, raw_data_mat).astype('float') ### None to nan
        raw_idx_mat = np.array(c7_input[fit_para['data']]['idx'])
        
        raw_slice_range = fit_para['slice_range']
        raw_peak_ind = c7_input[fit_para['data']]['peak_ind']
        if fit_para['buildup']['apply']:
            this_slice_range = deepcopy(raw_slice_range)
            this_slice_range[0] = raw_peak_ind - int(np.ceil(fit_para['buildup']['days']/30))
        else:
            this_slice_range = deepcopy(raw_slice_range)
        ret['slice_range'] = this_slice_range
        
        ret['entire_t_pred'] = raw_idx_mat[0,this_slice_range[0]:] - raw_idx_mat[0,this_slice_range[0]] + 14
        ret['data_mat'] = raw_data_mat[:, this_slice_range[0]:this_slice_range[1]]
        ret['idx_mat'] = raw_idx_mat[:, this_slice_range[0]:this_slice_range[1]]
        ret['TC_model'] = fit_para['TC_model']
        ret['TC_percentile'] = fit_para['TC_percentile']
        ret['para_dict'] = c7_input['para_dict']
        ret['cum_dict'] = c7_input['cum_dict']
        for k,v in ret['cum_dict'].items():
            ret['cum_dict'][k] = np.array(v)
        ret['eur'] = c7_input['eur_dict']['eur']
        
        ret['data'] = fit_para['data']
        
        this_peak_ind = c7_input[fit_para['data']]['peak_ind']
        if this_peak_ind < this_slice_range[0]:
            this_peak_ind = this_slice_range[0]
        
        ret['peak_ind'] = this_peak_ind - this_slice_range[0]
        ret['buildup'] = fit_para['buildup']
        return ret
    
    def analysis(self, T1_ret):
        ######################### class initialization
        det = get_DCA()
        det.set_seed(1)
        seg = segment_template()


        ######################### data stateement
        ##### data & idx
        data_mat = T1_ret['data_mat']
        idx_mat = T1_ret['idx_mat']
        ##### eur_related
        eurs = T1_ret['eur']
        ##### cum_related
        cum_pred_t_vec = T1_ret['cum_dict']['cum_pred_t_vec']
        cum_subind = T1_ret['cum_dict']['cum_subind']
        cum_plot_idx = T1_ret['cum_dict']['idx']
        ##### para_dict
        para_dict = T1_ret['para_dict']
        ##### fit_para
        TC_perc = T1_ret['TC_percentile']
        TC_model = T1_ret['TC_model']
        buildup = T1_ret['buildup']
        target_peakind = T1_ret['peak_ind']
        entire_pred_t = T1_ret['entire_t_pred']
        ret_slice_range = deepcopy(T1_ret['slice_range'])

        ######################### make data for bench mark
        t_vec = idx_mat[0,:] - idx_mat[0, 0] + 14
        t_peak = t_vec[target_peakind]
        percentile_data = np.apply_along_axis(lambda x: np.nanpercentile(x, TC_perc), 0, data_mat)
        align_data = np.zeros(percentile_data.shape)
        for i in range(len(TC_perc)):
            p10 = percentile_data[i,:]
            this_maxind = np.argmax(p10)
            this_left = np.zeros(target_peakind)
            this_right = np.zeros(p10.shape[0]-target_peakind)
            
            left_valid = np.min([target_peakind, this_maxind])
            this_left[(target_peakind-left_valid):(target_peakind)] = p10[(this_maxind-left_valid):this_maxind]
            
            right_valid = np.min([p10.shape[0] - this_maxind, p10.shape[0] - target_peakind])
            this_right[0:(0 + right_valid)] = p10[this_maxind:(this_maxind + right_valid)]
            this_fill = np.concatenate([this_left, this_right])
            align_data[i,:] = this_fill
        percentile_data = align_data
        
        idx_benchmark = np.argwhere(np.array(TC_perc) == 50)[0, 0]
        benchmark_prod = align_data[idx_benchmark,:]
        benchmark_data = np.stack([t_vec, benchmark_prod], axis=1)
        ######################### adjust the pred_t and entire_pred_t
        if buildup['apply']:
            if T1_ret['peak_ind'] > 0:
                k = 14 + buildup['days'] - t_vec[T1_ret['peak_ind'] - 1]
            else:
                k = 0
        else:
            k = 0

        if k==0:
            pred_t_vec = t_vec

            entire_pred_t += 0
        else:
            pred_t_vec = t_vec + k
            pred_t_vec[1:] = pred_t_vec[:-1]
            pred_t_vec[0] = 14

            entire_pred_t +=  k
            entire_pred_t[1:] = entire_pred_t[:-1]
            entire_pred_t[0] = 14
        
        ######################### get benchmark
        ##### fit
        label = 8
        p_range = det.get_range(TC_model,para_dict,label)
        TC_model, benchmark_p, loss, benchmark_p_fixed = det.get_params(benchmark_data, t_peak, label, model_name = TC_model, ranges = p_range)
        ##### buildup
        benchmark_p, benchmark_p_fixed = self.build_s[TC_model](benchmark_p,benchmark_p_fixed, buildup)
        
        ######################### analysis
        target_eurs = np.percentile(eurs, TC_perc)
        P_dict = {}
        cum_container = np.zeros(cum_plot_idx.shape)
        #####
        P50_fit = {'p':benchmark_p, 'p_fixed':benchmark_p_fixed}
        for i in range(len(TC_perc)):
            this_target_eur = target_eurs[i]
            this_perc = TC_perc[i]
            if TC_perc[i] !=50:
                this_value = percentile_data[i,:]
                this_value[np.isnan(this_value)] = 0
                this_data = np.stack([t_vec, this_value], axis = 1)
                TC_model, p, loss, p_fixed = det.get_params(this_data, t_peak, label, model_name = TC_model, ranges = p_range)
                p, p_fixed = self.adjust_p_s[TC_model](p, p_fixed, benchmark_p, benchmark_p_fixed)
            else:
                p = deepcopy(benchmark_p)
                p_fixed = deepcopy(benchmark_p_fixed)
            this_segments, this_eur = self.reach_eur_s[TC_model](p,p_fixed, this_target_eur, para_dict)
            this_pred = seg.predict(pred_t_vec, this_segments)
            this_name = 'P' + str(this_perc)
            P_dict[this_name] = {'parameter' : this_segments,'eur':this_eur, 'data' : this_pred, 'idx': pred_t_vec, 'slice_range' : ret_slice_range,'compare_range' : [None, None]}
            cum_dict = generate_cum(this_segments, cum_pred_t_vec, cum_container, cum_subind)

            P_dict[this_name].update(cum_dict)
           
        
        ##### check if crossing
        final_pred_mat = np.zeros((len(TC_perc), entire_pred_t.shape[0]))

        for i in range(len(TC_perc)):
            this_perc = TC_perc[i]
            this_name = 'P' + str(this_perc)
            this_pred = seg.predict(entire_pred_t, P_dict[this_name]['parameter'])
            final_pred_mat[i,:] = this_pred
        # return P_dict, final_pred_mat, entire_pred_t

        cp_base = np.arange(len(TC_perc))
        check = np.apply_along_axis(lambda x: np.argsort(x) == cp_base, 0, final_pred_mat)
        if np.all(check):
            print('stable')
            return P_dict
        else:
            print('trigger')
            p50_p = P50_fit['p']
            p50_p_fixed = P50_fit['p_fixed']
            for i in range(len(TC_perc)):
                this_perc = TC_perc[i]
                this_name = 'P' + str(this_perc)
                if this_name != 'P50':
                    this_target_eur = target_eurs[i]
                    this_segments, this_eur = self.reach_eur_s[TC_model](p50_p, p50_p_fixed, this_target_eur, para_dict)
                    this_pred = seg.predict(pred_t_vec, this_segments)
                    
                    P_dict[this_name] = {'parameter' : this_segments,'eur':this_eur, 'data' : this_pred, 'idx': pred_t_vec, 'slice_range' : ret_slice_range,'compare_range' : [None, None]}
                    cum_dict = generate_cum(this_segments, cum_pred_t_vec, cum_container, cum_subind)

                    P_dict[this_name].update(cum_dict)
            
            return P_dict
            
    

    def T2(self, ana_out):
        P_dict = ana_out
        for k,v in P_dict.items():
            P_dict[k]['data'] = P_dict[k]['data'].tolist()
            P_dict[k]['idx'] = P_dict[k]['idx'].tolist()
            P_dict[k]['single_cum'] = P_dict[k]['single_cum'].tolist()
            P_dict[k]['group_cum'] = P_dict[k]['group_cum'].tolist()
            for elem in v['parameter']:
                for kk,vv in elem.items():
                    if type(vv)!=str: 
                        elem[kk] = float(vv)
        
        return P_dict
    
    def body(self, c7_input):
        return self.T2(self.analysis(self.T1(c7_input)))
    

class c8:
    def __init__(self):
        self.random_seed = None
        self.maxite = 5
        self.build_s = build_s
        # self.adjust_p_s = adjust_p_s
        
    def update_seed(self,random_seed):
        self.random_seed = random_seed
    
    def update_maxite(self, maxite):
        self.maxite = maxite

        
    def T1(self, c8_input):
        ret = {}
        fit_para = c8_input['fit_para']
        
        raw_slice_range = fit_para['slice_range']
        raw_peak_ind = c8_input[fit_para['data']]['peak_ind']
        if fit_para['buildup']['apply']:
            this_slice_range = deepcopy(raw_slice_range)
            this_slice_range[0] = raw_peak_ind - int(np.ceil(fit_para['buildup']['days']/30))
        else:
            this_slice_range = deepcopy(raw_slice_range)
        ret['slice_range'] = this_slice_range
        
        
        raw_data_mat = np.array(c8_input[fit_para['data']]['data'])
        raw_data_mat = np.where(raw_data_mat==None, np.nan, raw_data_mat).astype('float')
        raw_idx_mat = np.array(c8_input[fit_para['data']]['idx'])
        ret['data_mat'] = raw_data_mat[:, this_slice_range[0]:this_slice_range[1]]
        ret['idx_mat'] = raw_idx_mat[:, this_slice_range[0]:this_slice_range[1]]
        
        ret['entire_t_pred'] = raw_idx_mat[0, this_slice_range[0]:] - raw_idx_mat[0, this_slice_range[0]] + 14
        
        ret['cum_dict'] = c8_input['cum_dict']
        for k,v in ret['cum_dict'].items():
            ret['cum_dict'][k] = np.array(v)
        
        ret['TC_model'] = fit_para['TC_model']
        ret['fit_percentile'] = fit_para['fit_percentile']
        ret['compare_range'] = fit_para['compare_range']
        ret['para_dict'] = c8_input['para_dict']

        ret['TC_para_name'] = fit_para['TC_para_name']
        
        this_peak_ind = c8_input[fit_para['data']]['peak_ind']
        if this_peak_ind < this_slice_range[0]:
            this_peak_ind = this_slice_range[0]
        
        ret['peak_ind'] = this_peak_ind - this_slice_range[0]
        ret['buildup'] = fit_para['buildup']
        return ret
    
    def analysis(self, T1_ret):
        ######################### class initialization
        self.update_seed(1)
        det = get_DCA()
        det.set_seed(1)
        seg = segment_template()
        ######################### data stateement
        ##### data & idx
        data_mat = T1_ret['data_mat']
        idx_mat = T1_ret['idx_mat']
        ##### eur_related
        
        ##### cum_related
        cum_subind = T1_ret['cum_dict']['cum_subind']
        cum_vecind = T1_ret['cum_dict']['cum_vecind']
        cum_target = T1_ret['cum_dict']['cum']
        cum_pred_t_vec = T1_ret['cum_dict']['cum_pred_t_vec']
        ##### para_dict
        para_dict = T1_ret['para_dict']
        
        ##### fit_para
        TC_model = T1_ret['TC_model']
        buildup = T1_ret['buildup']
        target_peakind = T1_ret['peak_ind']
        entire_pred_t = T1_ret['entire_t_pred']
        ret_slice_range = deepcopy(T1_ret['slice_range'])
        compare_range = T1_ret['compare_range']
        benchmark_percentile = T1_ret['fit_percentile']
        ######################### make data for bench mark
        t_vec = idx_mat[0,:] - idx_mat[0, 0] + 14
        t_peak = t_vec[target_peakind]
        this_value = np.nanpercentile(data_mat, benchmark_percentile, 0)
        ##### align
        p10 = this_value
        this_maxind = np.argmax(p10)
        this_left = np.zeros(target_peakind)
        this_right = np.zeros(p10.shape[0]-target_peakind)
        
        left_valid = np.min([target_peakind, this_maxind])
        this_left[(target_peakind-left_valid):(target_peakind)] = p10[(this_maxind-left_valid):this_maxind]
        
        right_valid = np.min([p10.shape[0] - this_maxind, p10.shape[0] - target_peakind])
        this_right[0:(0 + right_valid)] = p10[this_maxind:(this_maxind + right_valid)]
        this_fill = np.concatenate([this_left, this_right])
        benchmark_value = this_fill
        ##### make data
        benchmark_data = np.stack([t_vec, benchmark_value], axis=1)
        ######################### adjust the pred_t and entire_pred_t
        if buildup['apply']:
            if T1_ret['peak_ind'] > 0:
                k = 14 + buildup['days'] - t_vec[T1_ret['peak_ind']-1]
            else:
                k = 0
        else:
            k = 0

        if k==0:
            pred_t_vec = t_vec

            entire_pred_t += 0
        else:
            pred_t_vec = t_vec + k
            pred_t_vec[1:] = pred_t_vec[:-1]
            pred_t_vec[0] = 14

            entire_pred_t +=  k
            entire_pred_t[1:] = entire_pred_t[:-1]
            entire_pred_t[0] = 14
        ######################### get benchmark
        ##### qfit
        label = 8
        p_range = det.get_range(TC_model,para_dict,label) ## the range for type curve
        TC_model, benchmark_p, loss, benchmark_p_fixed = det.get_params(benchmark_data, t_peak, label, model_name = TC_model, ranges = p_range)
        ##### buildup
        benchmark_p, benchmark_p_fixed = self.build_s[TC_model](benchmark_p, benchmark_p_fixed, buildup)
        
        ######################### analysis
        start_idx = benchmark_p_fixed[0]
        start_date = datetime.date(1900,1,1) + datetime.timedelta(int(start_idx))
        start_year = start_date.year
        end_year = start_year + para_dict['well_life']
        end_date = datetime.date(end_year, start_date.month, start_date.day)
        t_end_life = (end_date - datetime.date(1900,1,1)).days

        para_insert_list= []
        this_range = []
        ## for final optimization, and move after benchmark is generated
        para_dict['q_peak'] = [np.nanpercentile(data_mat[:,target_peakind], 10) + 1, np.nanpercentile(data_mat[:,target_peakind], 90) + 1]
        for para in T1_ret['TC_para_name']:
            this_range += [tuple(para_dict[para])]
            this_dict = {}
            if para in model_p_name[TC_model]:
                this_dict['part'] = 'p'
                for i, name in enumerate(model_p_name[TC_model]):
                    if name == para:
                        this_dict['ind'] = i
            else:
                this_dict['part'] = 'p_fixed'
                for i, name in enumerate(model_p_fixed_name[TC_model]):
                    if name == para:
                        this_dict['ind'] = i
            
            para_insert_list += [this_dict]
        empty_cum_mat = np.zeros((data_mat.shape[0], cum_target.shape[0]))
        p2_seg_para = {
            't_first' : benchmark_p_fixed[0],
            't_end_data' : benchmark_p_fixed[0],
            't_end_life' : t_end_life,
            'q_final' : para_dict['q_final'],
            'D_lim_eff' : para_dict['D_lim_eff']
        }

        args = (compare_range, benchmark_p, benchmark_p_fixed, TC_model, para_insert_list,cum_pred_t_vec, cum_target, empty_cum_mat, cum_vecind, p2_seg_para)
        result = differential_evolution(self.alpha, args = args, bounds = this_range, seed = self.random_seed,maxiter=self.maxite)
        this_para = result.x
        this_p = deepcopy(benchmark_p)
        this_p_fixed = deepcopy(benchmark_p_fixed)
        for i in range(len(para_insert_list)):
            if para_insert_list[i]['part'] == 'p':
                this_p[para_insert_list[i]['ind']] = this_para[i]
            else:
                this_p_fixed[para_insert_list[i]['ind']] = this_para[i]
            
        
        this_segments = seg.p2seg_s[TC_model](this_p,this_p_fixed, p2_seg_para)
        ret_sum_mat = deepcopy(empty_cum_mat)
        
        
        cum_pred = seg.predict(cum_pred_t_vec, this_segments)
        for i in range(ret_sum_mat.shape[0]):
            ret_sum_mat[i, cum_subind[i,0]:cum_subind[i,1]] = cum_pred
        
        ret_sum = np.nansum(ret_sum_mat,axis=0)
        ret_cum = np.nancumsum(ret_sum)
        this_eur = seg.eur(0,t_vec[0]-100, this_segments[0]['start_idx'],this_segments[-1]['end_idx'] , this_segments)
        
#        print(pred_t_vec[:10])
        ret_pred = seg.predict(pred_t_vec, this_segments)
        ret = {'parameter' : this_segments, 'eur':this_eur, 'data' : ret_pred, 'idx': pred_t_vec, 'slice_range' : ret_slice_range, 'compare_range' : compare_range}
        ret['single_cum'] = np.nancumsum(ret['data'])
        ret['group_cum'] = ret_cum

        return ret
    
    def T2(self, ana_out):
        for elem in ana_out['parameter']:
            for k,v in elem.items():
                if type(v) != str:
                    elem[k] = float(v)
        
        ana_out['eur'] = float(ana_out['eur'])
        ana_out['data'] = ana_out['data'].tolist()
        ana_out['idx'] = ana_out['idx'].tolist()
        ana_out['single_cum'] = ana_out['single_cum'].tolist()
        ana_out['group_cum'] = ana_out['group_cum'].tolist()
        
        
        return ana_out
    
    def body(self, c8_input):
        return self.T2(self.analysis(self.T1(c8_input)))
    
    def alpha(self, para,*args):
        
        compare_range, benchmark_p, benchmark_p_fixed, TC_model, para_insert_list, cum_pred_t_vec, cum_target, empty_cum_mat, cum_vecind, p2_seg_para = args
        nr = int(cum_vecind.shape[0]/cum_target.shape[0])

        this_p = deepcopy(benchmark_p)
        this_p_fixed = deepcopy(benchmark_p_fixed)
        
        
        for i in range(len(para_insert_list)):
            if para_insert_list[i]['part'] == 'p':
                this_p[para_insert_list[i]['ind']] = para[i]
            else:
                this_p_fixed[para_insert_list[i]['ind']] = para[i]
        
        
        
        seg = segment_template()
        this_segments = seg.p2seg_s[TC_model](this_p,this_p_fixed, p2_seg_para)
        
        
        this_pred = seg.predict(cum_pred_t_vec, this_segments)
        #######################
        this_pred_rep = np.tile(this_pred, nr)
        this_pred_vec = np.zeros(cum_vecind.shape)
        this_pred_vec[cum_vecind] = this_pred_rep
        this_pred_mat= this_pred_vec.reshape((nr,-1))
        this_cum = this_pred_mat.sum(axis=0).cumsum()
        loss = np.mean(np.abs(cum_target[compare_range[0]:compare_range[1]] - this_cum[compare_range[0]:compare_range[1]]))

        
        return loss
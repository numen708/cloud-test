import warnings
import numpy as np
from forecast.helper import arps_sw, common_number

data_freq = common_number['days_in_year']
common_template = {
        'start_idx' : 0,
        'end_idx' : 31,
        'q_start' : 1,
        'q_end' : 1200
        }

exp_inc_template = {
        'name' : 'exp_inc',
        'slope' : 1,
        'D_eff' : -0.5,
        'D' : -0.67
        }

exp_dec_template = {
        'name' : 'exp_dec',
        'slope' : -1,
        'D_eff' : 0.5,
        'D' : 0.67
        }

arps_template = {
        'name' : 'arps',
        'slope': -1,
        'b' : 2,
        'D_eff' : 0.5,
        'D' : 0.67
        }

arps_modified_template = {
        'name' : 'arps_modified',
        'slope' : -1,
        'D_eff' : 0.5,
        'D' : 0.67,
        'b' : 2,
        'sw_idx' : 21.1,
        'q_sw' : 1230,
        'D_exp_eff' : 0.06,
        'D_exp' : 0.012
        }

flat_template = {
        'name' : 'flat',
        'slope' : 0,
        'c' : 123
        }

empty_template = {
        'name' : 'empty',
        'slope' : 0
        }

templates = {
        'common' : common_template,
        'exp_inc' : exp_inc_template,
        'exp_dec' : exp_dec_template,
        'arps': arps_template, 
        'arps_modified' : arps_modified_template,
        'flat' : flat_template,
        'empty' : empty_template}
############################################################################################# predict_s
def predict_exp_inc(raw_t, template):
    t = np.array(raw_t)
    start_idx = template['start_idx']
    end_idx = template['end_idx']
    D = template['D']
    q_start = template['q_start']

    if np.any((t > end_idx) & (t < start_idx)):
        warnings.warn('t out of range!')

    ret = np.zeros(t.shape)
    range_1 = (t <= end_idx) & (t >= start_idx)
    ret[range_1] = q_start * np.exp( - D * (t[range_1]-start_idx))
    return ret
        

def predict_exp_dec(raw_t, template):
    t = np.array(raw_t)
    start_idx = template['start_idx']
    end_idx = template['end_idx']
    D = template['D']
    q_start = template['q_start']

    if np.any((t > end_idx) & (t < start_idx)):
        warnings.warn('t out of range!')

    ret = np.zeros(t.shape)
    range_1 = (t <= end_idx) & (t >= start_idx)
    ret[range_1] = q_start * np.exp( - D * (t[range_1]-start_idx))
    return ret
    
def predict_arps(raw_t, template):
    t = np.array(raw_t)
    start_idx = template['start_idx']
    end_idx = template['end_idx']
    b = template['b']
    D = template['D']
    q_start = template['q_start']

    if np.any((t > end_idx) & (t < start_idx)):
        warnings.warn('t out of range!')

    ret = np.zeros(t.shape)
    range_1 = (t <= end_idx) & (t >= start_idx)
    ret[range_1] = q_start * np.power(1+b*D*(t[range_1]- start_idx), -1/b)
    return ret
        
def predict_arps_modified(raw_t, template):
    t = np.array(raw_t)
    start_idx = template['start_idx']
    end_idx = template['end_idx']
    sw_idx = template['sw_idx']
    
    q_start = template['q_start']
    b = template['b']
    D = template['D']

    q_sw = template['q_sw']
    D_exp = template['D_exp']
    
    if np.any((t > end_idx) & (t < start_idx)):
        warnings.warn('t out of range!')

    
    ret = np.zeros(t.shape)
    range_1 = (t >= start_idx) & (t <= np.min([sw_idx, end_idx]))
    range_2 = (t > np.min([sw_idx, end_idx])) & (t <= end_idx)
    
    ret[range_1] = q_start * np.power(1+b*D*(t[range_1]- start_idx), -1/b)
    ret[range_2] = q_sw * np.exp(-D_exp * (t[range_2] - sw_idx))
    return ret

def predict_flat(raw_t, template):
    t = np.array(raw_t)
    c = template['c']
    start_idx = template['start_idx']
    end_idx = template['end_idx']
    if np.any((t > end_idx) & (t < start_idx)):
        warnings.warn('t out of range!')

    ret = np.zeros(t.shape)
    range_1 = (t <= end_idx) & (t >= start_idx)
    ret[range_1] = c
    return ret

def predict_empty(raw_t, template):
    t = np.array(raw_t)
    start_idx = template['start_idx']
    end_idx = template['end_idx']
    if np.any((t > end_idx) & (t < start_idx)):
        warnings.warn('t out of range!')

    return np.zeros(t.shape)
    
segment_predict_s = {
            'exp_inc' : predict_exp_inc,
            'exp_dec' : predict_exp_dec,
            'arps' : predict_arps,
            'arps_modified' : predict_arps_modified,
            'flat' : predict_flat,
            'empty' : predict_empty
        }
############################################################################################# eurs
def integral_exp_inc(left_idx, right_idx, template):
    if (left_idx > template['end_idx'] + 1) | (right_idx < template['start_idx']) | (left_idx>=right_idx):
        return 0
    else:
        left_idx = np.max([left_idx, template['start_idx']])
        right_idx = np.min([right_idx, template['end_idx'] + 1])
        
        t0 = template['start_idx']
        D = template['D'] 
        q0 = template['q_start']
        ret = -q0/D * (np.exp(-D * (right_idx - t0)) - np.exp(-D * (left_idx - t0)))
        return ret

def integral_exp_dec(left_idx, right_idx, template):
    if (left_idx > template['end_idx'] + 1) | (right_idx < template['start_idx']) | (left_idx>=right_idx):
        return 0
    else:
        left_idx = np.max([left_idx, template['start_idx']])
        right_idx = np.min([right_idx, template['end_idx']+ 1])
        t0 = template['start_idx']
        D = template['D'] 
        q0 = template['q_start']
        ret = -q0/D * (np.exp(- D * (right_idx - t0)) - np.exp(-D * (left_idx - t0)))
        return ret

def integral_arps(left_idx, right_idx, template):
    if (left_idx > template['end_idx'] + 1) | (right_idx < template['start_idx'])| (left_idx>=right_idx):
        return 0
    else:
        left_idx = np.max([left_idx, template['start_idx']])
        right_idx = np.min([right_idx, template['end_idx']+ 1])
        b = template['b']
        D = template['D']
        q_start = template['q_start']
        t_start = template['start_idx']
        f_arps = lambda x: q_start * np.power(1 + b*D*(x - t_start), -1/b)
        q_s = f_arps(np.array([left_idx, right_idx]))
        
        q_left = q_s[0]
        q_right = q_s[1]
        return np.power(q_start,b)/(1-b)/D * (np.power(q_left, 1-b) - np.power(q_right, 1-b))

def integral_arps_modified(left_idx, right_idx, template):
    if (left_idx > template['end_idx'] + 1) | (right_idx < template['start_idx'])| (left_idx>=right_idx):
        return 0
    else:
        left_idx = np.max([left_idx, template['start_idx']])
        right_idx = np.min([right_idx, template['end_idx']+ 1])
        D = template['D']
        b = template['b']
        sw_idx = template['sw_idx']
        q_sw = template['q_sw']
        D_exp = template['D_exp']
        q_start = template['q_start']
        t_start = template['start_idx']
        q_sw = template['q_sw']
        f_arps = lambda x: q_start * np.power(1 + b*D*(x - t_start), -1/b)
        f_exp = lambda x: q_sw * np.exp(-D_exp*(x - sw_idx))
        ret = 0
        arps_idx_s = np.array([np.min([left_idx, sw_idx]), np.min([right_idx, sw_idx])])
        if arps_idx_s[0] < arps_idx_s[1]: 
            arps_q_s = f_arps(arps_idx_s)
            ret += np.power(q_start,b)/(1-b)/D * (np.power(arps_q_s[0], 1-b) - np.power(arps_q_s[1], 1-b))
        
        exp_idx_s = np.array([np.max([left_idx, sw_idx]), np.max([right_idx, sw_idx])])
        if exp_idx_s[0] < exp_idx_s[1]:
            exp_q_s = f_exp(exp_idx_s)
            ret += (exp_q_s[0] - exp_q_s[1])/D_exp
        
        return ret

def integral_flat(left_idx, right_idx, template):
    if (left_idx > template['end_idx'] + 1) | (right_idx < template['start_idx'])| (left_idx>=right_idx):
        return 0
    else:
        left_idx = np.max([left_idx, template['start_idx']])
        right_idx = np.min([right_idx, template['end_idx']+ 1])
        c = template['c']
        return c * (right_idx - left_idx)

def integral_empty(left_idx, right_idx, template):
    return 0
    
    
    
segment_integral_s = {
            'exp_inc' : integral_exp_inc,
            'exp_dec' : integral_exp_dec,
            'arps' : integral_arps,
            'arps_modified' : integral_arps_modified,
            'flat' : integral_flat,
            'empty' : integral_empty
        }
#############################################################################################  converts
def segment_arps_4_wp_p2seg(p,p_fixed, para_dict):
    ### @parameter para_dict: 't_end_life', 'q_final', 'D_lim_eff', 't_end_data'
    f_arps = lambda t,q,b,D,t0: q*np.power(1+b*D*(t - t0),-1/b)
    s = segment_template()
    #####
#    start_idx = para_dict['first_idx']
    t_end_life = para_dict['t_end_life']
    
    q_final = para_dict['q_final']
    D_lim_eff = para_dict['D_lim_eff']
    ### make sure the time index are all in order
    [q0,  D1, minus_t_elf_t_peak, b2] = p    
    [start_idx, minus_t0_t_start, minus_t_peak_t0, q_peak] = p_fixed
    t0 = start_idx + np.max([0,minus_t0_t_start])
    t_peak = t0 + np.max([0,minus_t_peak_t0])
    t_elf = t_peak + np.max([1,minus_t_elf_t_peak])
#    print(t_peak)
    b1 = 2
    if (t_peak==t0):
        D0 = -1
    else:
        D0 = -np.log(q_peak/q0)/(t_peak-t0)
    
    D0_eff = 1 - np.exp(-data_freq * D0)
    
    D1_eff = 1 - np.power(1 + data_freq * b1 * D1, -1/b1)
    t1_end = int(np.floor(t_elf))
    q1_end = f_arps(t1_end, q_peak, b1, D1, t_peak)
    q_elf = f_arps(t_elf, q_peak, b1, D1, t_peak)
    D_elf = q_peak/q_elf*D1*np.power(1+b1*D1*(t_elf-t_peak),-(1/b1 + 1))
    
    t2_start = t1_end + 1
    end_data_idx = np.max([para_dict['t_end_data'], t2_start])
    life_idx = np.max([end_data_idx, t_end_life])
#    print(life_idx)
    q2_start = f_arps(t2_start, q_elf, b2, D_elf, t_elf)
#    q2_start = q_elf*np.power(1+b2*D_elf*(t2_start - t_elf),-1/b2)
    D2 = D_elf/(1+b2 * D_elf * (t2_start - t_elf))
    D2_eff = 1 - np.power(1 + data_freq * b2 * D2, -1/b2)
    #####   q_start, b, D, D_lim_eff, idx_start, idx_end_data, life_idx
    idx_sw, D_exp, D_exp_eff = arps_sw(q2_start, b2, D2, D_lim_eff, t2_start, end_data_idx, life_idx) ### life_idx here is different from the t_end_life
    q_sw = f_arps(idx_sw, q2_start, b2, D2, t2_start)
    q_dataend = f_arps(end_data_idx, q2_start, b2, D2, t2_start)

    life_idx = np.max([end_data_idx, t_end_life])
    if q_dataend < q_final:
        t_final = end_data_idx
    else:
        if q_final >= q_sw:
            t_final = int(np.floor(t2_start + (np.power(q2_start/q_final, b2) - 1)/b2/D2))
        else:
            t_final = int(np.floor(idx_sw + np.log(q_sw/q_final)/D_exp))
    
    t_shut = np.min([t_final, life_idx])
    if t_shut <= idx_sw:
        q_shut = f_arps(t_shut, q2_start, b2, D2, t2_start)
    else:
        q_shut = q_sw * np.exp(-D_exp*(t_shut-idx_sw))
    
    
    
    ###
    ret = []
    ######################################################## first empty period
    
    if t0 > start_idx:
        empty_seg = s.get_template('empty')
        empty_seg['start_idx'] = start_idx
        empty_seg['end_idx'] = t0 - 1
        empty_seg['q_start'] = -1
        empty_seg['q_end'] = -1
        ret += [empty_seg]
    
    ####################################################### first exp_inc or exp_dec
    if t_peak > t0:
        if D0 < 0:
            exp_seg = s.get_template('exp_inc')
            
        else:
            exp_seg = s.get_template('exp_dec')
        exp_seg['start_idx'] = t0
        exp_seg['end_idx'] = t_peak - 1
        exp_seg['q_start'] = q0
        exp_seg['q_end'] = q0 * np.exp((-D0 * (exp_seg['end_idx'] - t0) ))
        exp_seg['D'] = D0
        exp_seg['D_eff'] = D0_eff
        ret += [exp_seg]
    
    ############################################## first arps
    arps_seg = s.get_template('arps')
    arps_seg['start_idx'] = t_peak
    arps_seg['end_idx'] = t1_end
    arps_seg['q_start'] = q_peak
    arps_seg['D'] = D1
    arps_seg['D_eff'] = D1_eff
    arps_seg['q_end'] = q1_end
    arps_seg['b'] = 2
    
    ret += [arps_seg]
    ############################################# arps_modified
    arps_modify_seg = s.get_template('arps_modified')
    arps_modify_seg['start_idx'] = t2_start
    arps_modify_seg['end_idx'] = t_shut
    arps_modify_seg['q_start'] = q2_start
    arps_modify_seg['D'] = D2
    arps_modify_seg['D_eff'] = D2_eff
    arps_modify_seg['q_end'] = q_shut
    arps_modify_seg['b'] = b2
    arps_modify_seg['sw_idx'] = idx_sw
    arps_modify_seg['q_sw'] = q_sw
    arps_modify_seg['D_exp_eff'] = D_exp_eff
    arps_modify_seg['D_exp'] = D_exp
    
    ret += [arps_modify_seg]
    
    return ret

p2seg_s = {'segment_arps_4_wp' : segment_arps_4_wp_p2seg}
import copy
class segment_template:
    def __init__(self):
        self.templates = templates
        self.p2seg_s = p2seg_s
        self.seg_predict_s = segment_predict_s
        self.seg_integral_s = segment_integral_s
        
    def get_template(self, name):
        com_dict = copy.deepcopy(self.templates['common'])
        name_dict = copy.deepcopy(self.templates[name])
        com_dict.update(name_dict)
        return com_dict

    def predict(self, raw_t, lists):
        t = np.array(raw_t)
        ret = np.zeros(t.shape)
        for seg in lists:
            this_range = (t<=seg['end_idx']) & (t>=seg['start_idx'])
            ret[this_range] = self.seg_predict_s[seg['name']](t[this_range], seg)
        
        return ret
    
    def eur(self, cum_data, end_data_idx, left_idx, right_idx, lists):
        left_idx = np.max([end_data_idx +1, left_idx])
        ret = cum_data
        for seg in lists:
            ret += self.seg_integral_s[seg['name']](left_idx, right_idx, seg)
        
        return ret
import numpy as np
import pandas as pd 
from joblib import load
from copy import deepcopy
from forecast.helper import converts
from keras import backend as K
from forecast.segment_templates import segment_template

class func2:
    def T1(self, group_info):
        group_info['table'] = pd.DataFrame(group_info['table'])
        return group_info
    
    def adjustor(self, whole_table, para_dict):
        #### this is only for segment_arps_4_wp
        #### requires the existence of range of 'D1'and 'b2' in para_dict
        use_table = deepcopy(whole_table)
        ### convert time related columns to integer
        best_minus_mask = use_table.columns.str.startswith('best_minus')
        use_table.loc[:,best_minus_mask] = np.ceil(use_table.loc[:,best_minus_mask])
        #### restrict the range of parameters
        for para in ['D1', 'b2']:
            this_col = use_table.columns.str.endswith(para)
            this_submat = np.array(use_table.loc[:,this_col])
            
            this_lower_mask = this_submat < para_dict[para][0]
#            print(this_lower_mask)
            this_submat[this_lower_mask] = para_dict[para][0]
            
            this_upper_mask = this_submat > para_dict[para][1]
            this_submat[this_upper_mask] = para_dict[para][1]
            
            
            use_table.loc[:,this_col] = deepcopy(this_submat)
            
        return use_table
        
    def analysis(self, T1_out):
        seg = segment_template()
        conv = converts()
        data_df = T1_out['table']
        para_dict = T1_out['para_dict']
        model_name = para_dict['model_name'] #### model_name = 'segment_arps_4_wp', in previous version
        percentile = para_dict['percentile']
        
        
        use_model = load(T1_out['model_download_str'])
        input_name = use_model['input_name']
        output_name = use_model['output_name']
        x_scaler = use_model['x_scaler']
        y_scaler = use_model['y_scaler']
        model = use_model['model']
        input_data = data_df[input_name]
        pred_scale = model.predict(x_scaler.transform(input_data))
        pred_raw = pd.DataFrame(y_scaler.inverse_transform(pred_scale), columns=output_name)
        whole_table = pd.concat([data_df, pred_raw], axis=1)
        flat_table = self.adjustor(whole_table, para_dict)

        ret_list = []
        for i in range(flat_table.shape[0]):
            this_dict = {}
            this_flat = flat_table.iloc[i,:]
#            print(this_flat)
            p2_seg_para = {
                    't_first' : this_flat['t_first'],
                    't_end_data' : this_flat['t_end_data'],
                    't_end_life' : this_flat['t_end_life'],
                    'q_final' : para_dict['q_final'],
                    'D_lim_eff' : para_dict['D_lim_eff']
                }
            
            this_P_dict = conv.flat_dict2hirchy_dict(this_flat, model_name, percentile, para_dict['prob_para'])
            this_P_dict['best'] = this_P_dict['P50']

            if this_flat['first_peak']:
                plot_idx = this_flat['t_first_valid_data']
            else:
                plot_idx = this_flat['t_peak']
                
            for k,v in this_P_dict.items():
                this_segs = seg.p2seg_s[model_name](v['p'], v['p_fixed'], p2_seg_para)
                ret_segs = []
                for this_seg in this_segs: ### delete the segments that's before the plot index
                    if this_seg['end_idx'] >= plot_idx:
                        ret_segs += [this_seg]
                this_P_dict[k] = ret_segs
            
            
            this_dict['P_dict'] = this_P_dict
            this_dict['p_extra'] = {'plot_idx' : plot_idx, 'well_id' : this_flat['well_id'], 'phase' : T1_out['phase']}
            ret_list += [this_dict]
        
        return ret_list
    
    def T2(self, ana_out_s):
        for ana_out in ana_out_s:
            P_dict = ana_out['P_dict']
            for k,v in P_dict.items():
                for elem in v:
                    for kk,vv in elem.items():
                        if type(vv)!=str: 
                            elem[kk] = float(vv)
            
            p_extra = ana_out['p_extra']
            p_extra['plot_idx'] = float(p_extra['plot_idx'])
        
        return ana_out_s
        
    def body(self, group_info):
        K.clear_session()
        return self.T2(self.analysis(self.T1(group_info)))


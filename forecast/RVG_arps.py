import numpy as np
import pandas as pd
from forecast.helper import common_number

data_freq = common_number['days_in_year']

def generate_D1_B2(num, para_dict):
    b1 = 2
    D1_eff_range = para_dict['D1_eff']
    D1_eff = np.random.rand(num) * (D1_eff_range[1]-D1_eff_range[0]) + D1_eff_range[0]
    D1 = (np.power((1-D1_eff),-b1)-1)/data_freq/b1
    #############################
    b2_range = para_dict['b2']
    b2 = np.random.rand(num)*(b2_range[1] - b2_range[0]) + b2_range[0]
    ret_df = pd.DataFrame({'D1':D1, 'b2':b2})    
    return ret_df

def generate_d_b(num, para_dict):
    D_eff_range = para_dict['D_eff']
    D_eff = np.random.rand(num) * (D_eff_range[1]-D_eff_range[0]) + D_eff_range[0]
    
    #############################
    b_range = para_dict['b']
    b = np.random.rand(num)*(b_range[1] - b_range[0]) + b_range[0]
    D = (np.power((1-D_eff),-b)-1)/data_freq/b
    ret_df = pd.DataFrame({'D':D, 'b':b})    
    return ret_df

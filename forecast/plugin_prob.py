import numpy as np
from forecast.skeleton_DCA_v3 import get_DCA

def segment_arps_4_wp_keep_ratio(transformed_data, pred, p_best,p_fixed_best, max_keep_per = 15, err_type = 'ra_abs_ratio', a=0.35, b=3):
    det_1 = get_DCA()
    min_keep_per = a* det_1.errorfunc_s[err_type](transformed_data[:,1],pred)*100 + b
    t_elf = p_best[2]
    t_end = transformed_data[-1,0]
    t1 = p_fixed_best[1]
    ratio = (t_elf-t1)/(t_end-t1)
    keep_ratio = max_keep_per - (max_keep_per-min_keep_per)*(ratio - 1)**2
    keep_ratio/=100
    return keep_ratio

def arps_wp_keep_ratio(transformed_data, pred, p_best,p_fixed_best, max_keep_per = 20, err_type = 'mae_std', a=0.35, b=3):
    det_1 = get_DCA()
    keep_ratio = max_keep_per* np.sqrt(det_1.errorfunc_s[err_type](transformed_data[:,1],pred))/100
    if keep_ratio<0.05:
        keep_ratio = 0.05
    
    return keep_ratio

keep_ratio_s = {'segment_arps_4_wp':segment_arps_4_wp_keep_ratio, 'arps_wp':arps_wp_keep_ratio}


########### 

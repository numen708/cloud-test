import numpy as np

def arps_wp(t,p,p_fixed):
    [b1,D1] = p
    [t_first, minus_t_peak_t_first, q_peak] = p_fixed
    t_peak = t_first + minus_t_peak_t_first
    return q_peak * np.power(1+b1*D1*(t-t_peak),-1/b1)
    

def duong(t,p,p_fixed):
    [q1] = p
    [t0, a, m] = p_fixed
    
    return q1*np.power(t-t0+1,-m) *np.exp(a/(1-m) * (np.power(t-t0+1,1-m)-1))

def logistic(t,p,p_fixed):
    [k,n,a] = p
    [t0] = p_fixed
    return k*n*a*np.power(t-t0,n-1)/np.power(a+np.power(t-t0,n),2)

def SEDM(t,p,p_fixed):
    [q1,tau, n] = p
    [t0] = p_fixed
    return q1*np.exp(-np.power((t-t0)/tau,n))

def power(t,p,p_fixed):
    [qi,Dinf,Di,n] = p
    [t0] = p_fixed
    return qi * np.exp(-Dinf*(t-t0) - Di/n * np.power(t-t0,n))


def segment_arps_4_wp(t,p,p_fixed):
    [q0,  D1, minus_t_elf_t_peak, b2] = p    
    [t_first, minus_t0_t_first, minus_t_peak_t0, q_peak] = p_fixed
    t0 = t_first + minus_t0_t_first
    t_peak = t0 + minus_t_peak_t0
    t_elf = t_peak + minus_t_elf_t_peak
    b1 = 2
    
    ret_list = np.zeros(t.shape)
    range_0 =  (t < t_peak)
    range_1 = (t >= t_peak) & (t < t_elf)
    range_2 = (t >= t_elf)
    if t_peak==t0:
        D0 = -1
    else:
        D0 = -np.log(q_peak/q0)/(t_peak-t0)
        
    ret_list[range_0] = q0 * np.exp(-D0*(t[range_0]-t0))

    ret_list[range_1] = q_peak*np.power(1+b1 * D1 * (t[range_1] - t_peak),-1/b1)
    q2 = q_peak*np.power(1+b1*D1*(t_elf - t_peak),-1/b1)
    D2 = q_peak/q2*D1*np.power(1+b1*D1*(t_elf-t_peak),-(1/b1 + 1))
    
    ret_list[range_2] = q2*np.power(1+b2*D2*(t[range_2]-t_elf),-1/b2)
    
    return ret_list


func_s = {'arps_wp':arps_wp, 'duong':duong, 'logistic':logistic,'SEDM':SEDM, 'power':power,
          'segment_arps_4_wp':segment_arps_4_wp}
########################################################################################### prepares
def arps_wp_prepare(data, t_peak, transformation):
    transformed_data = transformation(data, t_peak)
    p_fixed = np.array([data[0,0], t_peak - data[0,0], data[data[:,0]==t_peak,1]])
    p_range = [(1e-5,2), (1e-5, 10)]
    return p_fixed, p_range, transformed_data

def duong_prepare(data,t_peak, transformation):
    transformed_data = transformation(data, t_peak)
    t = transformed_data[:,0]
    t0 = t_peak
    
    rate = transformed_data[:,1]
    cum_rate = np.cumsum(rate)
    
    log_ratio = np.log(rate / cum_rate)
    log_t = np.log(t-t0+1)
    
    n = len(log_t)
    minus_m = ( n*  np.sum(np.multiply(log_t, log_ratio)) - np.sum(log_t) * np.sum(log_ratio) ) / ( n * np.sum(log_t ** 2) - np.sum(log_t) ** 2 )
    log_a = ( np.sum(log_ratio) - minus_m * np.sum(log_t) ) / n
    
    p_fixed = np.array([t0, np.exp(log_a), -minus_m])
    
    q_min = np.min(transformed_data[:,1])/2
    q_max = np.max(transformed_data[:,1])*2
    
    p_range = [(q_min, q_max)]
    
    return p_fixed, p_range, transformed_data

def logistic_prepare(data,t_peak,transformation):
    transformed_data = transformation(data,t_peak)
    p_fixed = np.array([t_peak - 1])
    p_range = [(100,10000000), (1e-5,10), (1e-5, 10)]
    return p_fixed, p_range, transformed_data

def SEDM_prepare(data,t_peak,transformation):
    transformed_data = transformation(data, t_peak)    
    p_fixed = np.array([t_peak])
    q_min = np.min(transformed_data[:,1])
    q_max = np.max(transformed_data[:,1])
    
    p_range = [(q_min/2, 2*q_max), (0.000001,10000), (0.006, 10)]
    # q1, tau, n
    return p_fixed, p_range, transformed_data
    
def power_prepare(data,t_peak,transformation):
    transformed_data = transformation(data,t_peak)
    p_fixed = np.array([t_peak])
    p_range = [(1000,10000000), (1e-9,1), (0.0001, 1),(0.02,2)]
    return p_fixed, p_range , transformed_data


def segment_arps_4_wp_prepare(data, t_peak, transformation):
    transformed_data = transformation(data,t_peak)
    t0 = np.min(transformed_data[:,0])
    t_first = data[0,0]
    t_peak = t_peak
    q_peak = data[data[:,0]==t_peak,1]
    p_fixed = np.array([t_first, t0 - t_first, t_peak - t0, q_peak])
    
    q0_left = 1
    if q0_left > q_peak:
        q0_left = np.min(transformed_data[:,1])
    
    p_range = [(q0_left,q_peak), (1e-5,5), (0, np.max(transformed_data[:,0])-t_peak), (1e-5,2)]
    return p_fixed, p_range, transformed_data


prepare_s = {'arps_wp':arps_wp_prepare, 'duong':duong_prepare, 'logistic':logistic_prepare, 'SEDM':SEDM_prepare,'power':power_prepare,
             'segment_arps_4_wp':segment_arps_4_wp_prepare}
#################################################################### prediction_s
arps_wp_pred = arps_wp
duong_pred = duong
logistic_pred = logistic
SEDM_pred = SEDM
power_pred = power
segment_arps_4_wp_pred = segment_arps_4_wp
predict_s = {'arps_wp':arps_wp_pred, 'duong':duong_pred, 'logistic':logistic_pred, 'SEDM':SEDM_pred, 'power':power_pred,
             'segment_arps_4_wp':segment_arps_4_wp_pred}



###################################################################### error functions 
def mse(y_true,y_pred):
    return np.mean(np.square(y_true-y_pred))

def mae(y_true,y_pred):
    return np.mean(np.abs(y_true-y_pred))

def ra(y_true,y_pred):
    ave = (y_true+y_pred)/2
    return np.mean(np.abs(y_true-y_pred)/ave)

def ra_weight(y_true, y_pred):
    n = y_true.shape[0]
    ave = (y_true + y_pred) / 2
    weight = np.ones((n,))
    if n > 24:
        weight[-6:,] *=5
    else:
        n_adjust = int(n/4)
        weight[-n_adjust:] *=5
    weight /= np.sum(weight)
    return np.dot(weight, np.abs(y_true - y_pred) / ave)
    
def ra_abs_ratio(y_true,y_pred):
    all_loss = np.square((y_true-y_pred)/(0.001+np.mean(np.abs(y_true-np.mean(y_true)))))
    loss1 = all_loss[:int(3*len(all_loss)/4)]
    loss2 = all_loss[int(3*len(all_loss)/4):]
    loss = (sum(loss1) + sum(np.multiply(loss2, 5)) )/ (len(loss1) + 5*len(loss2))
    return loss

def r_square_c(y_true,y_pred):
    return np.sum(np.square(y_true-y_pred))/np.sum(np.square(y_true-np.mean(y_true)))

def mae_std(y_true,y_pred):
    return np.mean(np.abs(y_true-y_pred))/np.std(y_true)

def weighted_mae(y_true,y_pred):
    n = y_true.shape[0]
    weight = np.linspace(1,4,n)
    weight /= np.sum(weight)
    return np.dot(weight, np.abs(y_true-y_pred))
    
def weighted_mae_1(y_true, y_pred):
    n = y_true.shape[0]
    weight = np.ones((n,))
    if n > 24:
        weight[-6:,] *=5
    else:
        n_adjust = int(n/4)
        weight[-n_adjust:] *=5
    weight/=np.sum(weight)
    return np.dot(weight, np.abs(y_true-y_pred))
    
errorfunc_s = {'mse':mse, 'mae':mae, 'ra':ra, 'ra_weight' : ra_weight, 'ra_abs_ratio':ra_abs_ratio, 'r_square_c':r_square_c, 'mae_std':mae_std,
               'weighted_mae':weighted_mae, 'weighted_mae_1':weighted_mae_1}


##################################################################### filter functions
def transformation_one(data,t_peak, thresh_high = 3, thresh_low = 0.3):
    this_data = data[data[:,1]!=0,:]
    t = this_data[:,0]
    rate = this_data[:,1]
    
    if len(t) < 5:
#        warnings.warn('Transformation one: number of data points is at least 5')
        return np.array([t, rate]).transpose()
    
    else:
        outlier_index = []
        
        for i in range(len(t)):
            
            if i == 0:
                r1 = rate[i]/rate[i+1]
                r2 = rate[i]/rate[i+2]
                ratios = np.array([r1, r2])
                if sum(ratios > 2.5) == 2:
                    outlier_index.append(i)
            
            elif i == 1:
                r1 = rate[i]/rate[i-1]
                r2 = rate[i]/rate[i+1]
                r3 = rate[i]/rate[i+2]
                ratios = np.array([r1, r2, r3])
                if sum(ratios > thresh_high) == 3:
                    outlier_index.append(i)
                elif sum(ratios < thresh_low) == 3:
                    outlier_index.append(i) 

            elif i == len(t)-2:
                r1 = rate[i]/rate[i-2]
                r2 = rate[i]/rate[i-1]
                r3 = rate[i]/rate[i+1]
                ratios = np.array([r1, r2, r3])
                if sum(ratios > thresh_high) == 3:
                    outlier_index.append(i)
                elif sum(ratios < thresh_low) == 3:
                    outlier_index.append(i)
                
            elif i == len(t)-1:
                r1 = rate[i]/rate[i-2]
                r2 = rate[i]/rate[i-1]
                ratios = np.array([r1, r2])
                if sum(ratios > thresh_high) == 2:
                    outlier_index.append(i)
                elif sum(ratios < thresh_low) == 2:
                    outlier_index.append(i)
                
            else:
                r1 = rate[i]/rate[i-2]
                r2 = rate[i]/rate[i-1]
                r3 = rate[i]/rate[i+1]
                r4 = rate[i]/rate[i+2]
                
                ratios = np.array([r1, r2, r3, r4])
                
                if sum(ratios > thresh_high) >= 3:
                    outlier_index.append(i)
                elif sum(ratios < thresh_low) >= 3:
                    outlier_index.append(i)
        
        t = [t[i] for i in range(len(t)) if i not in outlier_index]
        rate = [rate[i] for i in range(len(rate)) if i not in outlier_index]
        
        transformed_data = np.array([t, rate]).transpose()
        
        return transformed_data


def transformation_three(data, t_peak):
    transformed_data = transformation_one(data,t_peak)
    return transformed_data[transformed_data[:,0]>=t_peak,:]

transform_s = {'default':transformation_one, 'afterpeak':transformation_three}

default_transform_type = {
                         'arps_wp':'afterpeak',
                         'duong' : 'afterpeak', 
                         'logistic' : 'afterpeak',
                         'SEDM' : 'afterpeak',
                         'power' : 'afterpeak',
                         'segment_arps_4_wp' : 'default'
                         }


##################################### 

model_p_name = {
                'arps_wp':['b1','D1'],
                'duong' : ['q1'], 
                'logistic' : ['k','n','a'],
                'SEDM' : ['q1','tau','n'],
                'power' : ['qi','Dinf','Di','n'],
                'segment_arps_4_wp': ['q0','D1','minus_t_elf_t_peak','b2'],
             }


model_p_fixed_name = {
                      'arps_wp':['t_first','minus_t_peak_t_first', 'q_peak'],
                      'duong' : ['t0','a','m'], 
                      'logistic' : ['t0'],
                      'SEDM' : ['t0'],
                      'power' : ['t0'],
                      'segment_arps_4_wp': ['t_first', 'minus_t0_t_first','minus_t_peak_t0','q_peak']
        }

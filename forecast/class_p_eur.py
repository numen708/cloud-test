import numpy as np
from forecast.helper import common_number

class p_eur:
    def __init__(self):
        self.data_freq = common_number['days_in_year']

    def D_sw_finder_arps(self, q_start, b, D, D_lim_eff, idx_start, idx_end_data, t_end_life):
        ## vectorized input:  idx_start, q_start, D, b
        ## scalar input:  end_data_idx, D_lim_eff
        ## this case end_data_idx is the alwasy >= t0
        idx_last =  np.tile([idx_end_data], idx_start.shape[0]) # max([end_data, t0])
        D_last = D/(1+b*D*(idx_last - idx_start))
        D_lim = (np.power(1 - D_lim_eff, -b) - 1) / self.data_freq / b

        range_1 = (D_last > D_lim)
        range_2 = (D_last <= D_lim)
        idx_sw = np.zeros(D.shape)
        idx_sw[range_1] = (D[range_1] / D_lim[range_1] - 1) / (b[range_1] * D[range_1]) + idx_start[range_1]
        idx_sw[range_2] = idx_last[range_2]
        
        D_sw = D/(1 + b * D * (idx_sw - idx_start))
        D_exp = D_sw
        return D_sw, D_exp, idx_sw
    
    def segment_arps_4_wp_pred(self, t, p_table, p_fixed_table, q_final, D_lim_eff, data_end_idx, t_end_life):
        q0 = p_table[:,0]
        D1 = p_table[:,1]
        minus_t_elf_t_peak = p_table[:,2]
        b2 = p_table[:,3]
        t_first = p_fixed_table[:,0]
        minus_t0_t_first = p_fixed_table[:,1]
        minus_t_peak_t0 = p_fixed_table[:,2]
        q_peak = p_fixed_table[:,3]
        t0 = t_first + minus_t0_t_first
        t_peak = t0 + minus_t_peak_t0
        t_elf = t_peak + minus_t_elf_t_peak
        #################
        b1 = 2
        # D0 = -np.ones(p_table.shape[0])
        # D0[t_peak!=t0] = -np.log((q_peak/q0)[t_peak!=t0])/(t_peak-t0)[t_peak!=t0]
        q_elf = q_peak*np.power(1+b1*D1*(t_elf - t_peak),-1/b1)
        D2 = q_peak/q_elf*D1*np.power(1+b1*D1*(t_elf-t_peak),-(1/b1 + 1))
        #################  q_start, b, D, D_lim_eff, idx_start, idx_end_data, t_end_life
        D_sw, D_sw_exp, t_sw = self.D_sw_finder_arps(q_elf, b2, D2, D_lim_eff, t_elf, data_end_idx, t_end_life)
        ################
        t_mat = np.array([t] * p_table.shape[0])
        ####
        D1_mat = np.concatenate([D1.reshape(-1,1)] * t_mat.shape[1], axis =1)
        t_sw_mat = np.concatenate([t_sw.reshape(-1,1)] * t_mat.shape[1], axis =1)
        t_elf_mat = np.concatenate([t_elf.reshape(-1,1)] * t_mat.shape[1], axis =1)
        q_elf_mat = np.concatenate([q_elf.reshape(-1,1)] * t_mat.shape[1], axis =1)
        b2_mat = np.concatenate([b2.reshape(-1,1)] * t_mat.shape[1], axis =1)
        D2_mat = np.concatenate([D2.reshape(-1,1)] * t_mat.shape[1], axis =1)
        D_sw_exp_mat = np.concatenate([D_sw_exp.reshape(-1,1)] * t_mat.shape[1], axis =1)
        t_peak_mat = np.concatenate([t_peak.reshape(-1,1)] * t_mat.shape[1], axis =1)
        q_peak_mat = np.concatenate([q_peak.reshape(-1,1)] * t_mat.shape[1], axis =1)
        ###
        range_1 = (t_mat>=t_peak_mat) & (t_mat < t_elf_mat)
        range_2 = (t_mat>=t_elf_mat) & (t_mat < t_sw_mat)
        range_3 = (t_mat >= t_sw_mat)
        ###
        ret = np.zeros(t_mat.shape)
        ###
        ret[range_1] = q_peak_mat[range_1] * np.power(1 + b1 * D1_mat[range_1] * (t_mat[range_1] - t_peak_mat[range_1]), -1/b1)
        ret[range_2] = q_elf_mat[range_2] * np.power(1 + b2_mat[range_2] * D2_mat[range_2]*(t_mat[range_2] - t_elf_mat[range_2]), -1/b2_mat[range_2])
        q_sw = np.zeros(ret.shape)
        q_sw[range_3] = q_elf_mat[range_3] * np.power(1 + b2_mat[range_3] * D2_mat[range_3]*(t_sw_mat[range_3] - t_elf_mat[range_3]), -1/b2_mat[range_3])
        ret[range_3] = q_sw[range_3] * np.exp(-D_sw_exp_mat[range_3] * (t_mat[range_3] - t_sw_mat[range_3]))
#        ret[ret<q_final] = 0
        return ret
    
        